<?php

namespace Marcosgdf\EnlaceKeme\Tests\TestInterfaces;


use Marcosgdf\EnlaceKeme\Apunte;

interface InvoiceTestInterface
{

	/**
	 * Devuelve el ID de la factura
	 *
	 * @return int
	 */
	public function getId();

	/**
	 * Devuelve la fecha de la factura
	 *
	 * @return \DateTime
	 */
	public function getDate();

	/**
	 * Devuelve el concepto de la factura
	 *
	 * @return string
	 */
	public function getConcepto();

	/**
	 * Devuelve el array que describe la factura
	 *
	 * @param array $cache_re Array que contiene los tipos impositivos del recargo de equivalencia
	 * @return \Marcosgdf\EnlaceKeme\InvoiceDescriptor
	 */
	public function getDescriptor(array $cache_re);

	/**
	 * Devuelve un array con las cuentas que hacen la contrapartida del asiento
	 *
	 * @return array
	 */
	public function getContrapartidas();

	/**
	 * Devuelve el nombre del documento de la factura
	 *
	 * @return string
	 */
	public function getDocumento();

	/**
	 * Devuelve los apuntes generados para la factura
	 *
	 * @return Apunte[]
	 */
	public function getApuntes();

	/**
	 * Devuelve el formato exportado para EnlaceKeme de esta factura
	 *
	 * @return string
	 */
	public function getExpectedFormat();

}