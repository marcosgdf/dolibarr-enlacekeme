<?php

use Marcosgdf\EnlaceKeme\Bag\CommonBag;
use Marcosgdf\EnlaceKeme\EnlaceKeme;
use Marcosgdf\EnlaceKeme\InvoiceDescriptor;
use Marcosgdf\EnlaceKeme\Format\ApunteFormat;

class EnlaceKemeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var EnlaceKeme
     */
    public static $ek;

    public static function setUpBeforeClass()
    {
        global $langs, $conf, $db;

        if (!defined('MAIN_DB_PREFIX')) {
            define('MAIN_DB_PREFIX', 'llx_');
        }
        if (!defined('DOL_DOCUMENT_ROOT')) {
            define('DOL_DOCUMENT_ROOT', '../');
        }

        require_once __DIR__.'/../enlacekeme/class/EnlaceKeme.class.php';

        self::$ek = new EnlaceKeme();

        self::$ek->cache_re = array(
            21 => array(
                'sell' => '47702152',
                'buy' => '47202152'
            ),
            10 => array(
                'sell' => '47701014',
                'buy' => '47201014'
            ),
            4 => array(
                'sell' => '47700405',
                'buy' => '47200405'
            )
        );
        self::$ek->cache_kemeref = array(
            21 => array(
                'ref' => 'GN',
                'ref_re' => 'GNR'
            ),
            10 => array(
                'ref' => 'RD',
                'ref_re' => 'RD2'
            ),
            4 => array(
                'ref' => 'SRD',
                'ref_re' => 'SRD2'
            ),
        );

        require_once '/Users/marcos/dev/dolibarr/htdocs/core/class/translate.class.php';
        require_once '/Users/marcos/dev/dolibarr/htdocs/core/class/conf.class.php';

        $conf = new Conf();
        $langs = new Translate('/Users/marcos/dev/dolibarr/htdocs/langs/', $conf);

        $conf->global = (object)array();
        $conf->global->ENLACEKEME_CUENTA_HAEAT_IRPF = '47510000';

        //Cuenta por defecto para productos sin cuenta definida
        $conf->global->COMPTA_PRODUCT_SOLD_ACCOUNT = '70000000';

        //Cuenta por defecto para servicios sin cuenta definida
        $conf->global->COMPTA_SERVICE_SOLD_ACCOUNT = '70500000';

        //Cuenta por defecto para productos sin cuenta definida
        $conf->global->COMPTA_PRODUCT_BUY_ACCOUNT = '60000000';

        //Cuenta por defecto para servicios sin cuenta definida
        $conf->global->COMPTA_SERVICE_BUY_ACCOUNT = '62900000';

        //Cuenta por defecto para clientes sin cuenta definida
        $conf->global->COMPTA_ACCOUNT_CUSTOMER = '43000000';

        //Cuenta por defecto para proveedores sin cuenta definida
        $conf->global->COMPTA_ACCOUNT_SUPPLIER = '40000000';

        //Cuenta por defecto para proveedores sin cuenta definida
        $conf->global->COMPTA_VAT_ACCOUNT = '40000000';

        //Cuenta por defecto para devoluciones de ventas
        $conf->global->ENLACEKEME_CUENTA_DEVO_VENTAS = '70800000';

        //Cuenta por defecto para devoluciones de compras
        $conf->global->ENLACEKEME_CUENTA_DEVO_COMPRAS = '60800000';

        //Cuenta por defecto para retenciones de IRPF
        $conf->global->ENLACEKEME_CUENTA_RET_IRPF = '47300000';

        //Prefijo para la cuenta de anticipos.
        //En un anticipo será PREFIJO+CODIGO CLIENTE
        $conf->global->ENLACEKEME_PREFIJOCUENTA_ANTICIPOS = '123';

        require_once 'MockDoliDB.php';

        $db = new MockDoliDB();

        require 'MockSociete.php';
    }

    public function setUp()
    {
        self::$ek->return_diario = new CommonBag('{diario}');
        self::$ek->return_librofacturas = new CommonBag('{libro_facturas}');
        self::$ek->return_plancontable = new CommonBag('{plancontable}');
        self::$ek->return_datosauxiliares = new CommonBag('{datos_auxiliares}');
        self::$ek->apunte_counter = 1;
        self::$ek->asiento_counter = 1;
    }

    /**
     * @covers Marcosgdf\EnlaceKeme\EnlaceKeme::isCuentaCliente
     */
    public function testIsCuentaCliente()
    {
        $cuenta = '430000000001';

        $this->assertTrue(EnlaceKeme::isCuentaCliente($cuenta));

        $cuenta = '4000000';

        $this->assertFalse(EnlaceKeme::isCuentaCliente($cuenta));
    }

    /**
     * @covers Marcosgdf\EnlaceKeme\EnlaceKeme::getResult
     */
    public function testGetResult()
    {
        self::$ek->return_diario = new CommonBag('{diario}');
        self::$ek->return_librofacturas = new CommonBag('{libro_facturas}');
        self::$ek->return_plancontable = new CommonBag('{plancontable}');
        self::$ek->return_datosauxiliares = new CommonBag('{datos_auxiliares}');

        $expected = "{plancontable}
\n{datos_auxiliares}
\n{diario}
\n{libro_facturas}\n";

        $this->assertEquals($expected, self::$ek->getResult());

    }

    /**
     * @covers Marcosgdf\EnlaceKeme\EnlaceKeme::restarFactura
     */
    public function testRestarFactura()
    {
        /**
         * Retratamos la siguiente factura:
         *
         * Venta producto A (70000001) => 400 €
         * Venta producto B (70000002) => 400 €
         * Venta producto C (70000003) => 10 €
         * Total IVA:
         * - 21 % (A+B) (477000021) => 168
         * - 10 % (C) (477000010) => 1
         * Total RE:
         * - 5,2 % (A+B) (477002152) => 41,6
         * - 0,4 % (C) (477001004) => 0,04
         * Total IRPF:
         * - 21 % (475100021) => 168
         * - 10 % (C) (475100010) => 1
         *
         * Total factura: 851,64 €
         */

        $factura1 = new InvoiceDescriptor();

        $factura1->date = '20140101010101';
        $factura1->ref = 'FA2014/0001';
        $factura1->type = InvoiceDescriptor::TYPE_GENERAL;
        $factura1->total_ht = 810;
        $factura1->total_ttc = 851.64;
        $factura1->bases = array (
            70000001 => 400,
            70000002 => 400,
            70000003 => 10
        );
        $factura1->iva = array(
            477000021 => array(
                'cuenta_base' => '',
                'tx' => 21,
                'retx' => 0,
                'bi' => 800,
                'amount' => 168,
                'reamount' => 0
            ),
            477000010 => array(
                'cuenta_base' => '70000003',
                'tx' => 10,
                'retx' => 0,
                'bi' => 10,
                'amount' => 1,
                'reamount' => 0
            ),
        );
        $factura1->localtax1 = array(
            477002152 => 41.6,
            477001004 => 0.4
        );
        $factura1->localtax2 = array(
            475100021 => 168,
            475100010 => 1
        );

        /**
         * Retratamos la siguiente factura:
         *
         * Venta producto A (70000001) => 300 €
         * Venta producto B (70000002) => 400 €
         * Venta producto C (70000003) => 10 €
         * Total IVA:
         * - 21 % (A+B) (477000021) => 147
         * - 10 % (C) (477000010) => 1
         * Total RE:
         * - 5,2 % (A+B) (477002152) => 36,4
         * - 0,4 % (C) (477001004) => 0,04
         * Total IRPF:
         * - 21 % (475100021) => 147
         * - 10 % (C) (475100010) => 1
         *
         * Total factura: 746.4 €
         */

        $factura2 = new InvoiceDescriptor();
        $factura2->date = '20140101010101';
        $factura2->ref = 'FA2014/0001';
        $factura2->type = InvoiceDescriptor::TYPE_GENERAL;
        $factura2->total_ht = 710;
        $factura2->total_ttc = 746.4;
        $factura2->bases = array(
            70000001 => 300,
            70000002 => 400,
            70000003 => 10
        );
        $factura2->iva = array(
            477000021 => array(
                'cuenta_base' => '',
                'tx' => 21,
                'retx' => 0,
                'bi' => 700,
                'amount' => 147,
                'reamount' => 0
            ),
            477000010 => array(
                'cuenta_base' => '70000003',
                'tx' => 10,
                'retx' => 0,
                'bi' => 10,
                'amount' => 1,
                'reamount' => 0
            ),
        );
        $factura2->localtax1 = array(
            477002152 => 36.4,
            477001004 => 0.4
        );
        $factura2->localtax2 = array(
            475100021 => 147,
            475100010 => 1
        );

        //Resultado esperado: factura1-factura2

        $expected = new InvoiceDescriptor();
        $expected->total_ht = 100;
        $expected->total_ttc = 105.24;
        $expected->bases = array(
            70000001 => 100,
            70000002 => 0,
            70000003 => 0,
        );
        $expected->iva = array(
            477000021 => array(
                'cuenta_base' => '',
                'tx' => 21,
                'retx' => 0,
                'bi' => 100,
                'amount' => 21,
                'reamount' => 0
            ),
            477000010 => array(
                'cuenta_base' => '70000003',
                'tx' => 10,
                'retx' => 0,
                'bi' => 0,
                'amount' => 0,
                'reamount' => 0
            ),
        );
        $expected->localtax1 = array(
            477002152 => 5.2,
            477001004 => 0
        );
        $expected->localtax2 = array(
            475100021 => 21,
            475100010 => 0
        );

        $this->assertEquals($expected, EnlaceKeme::restarFactura($factura1, $factura2));

        //Resultado esperado: factura2-factura1 (lo mismo pero en negativo)

        $expected = new InvoiceDescriptor();
        $expected->total_ht = -100;
        $expected->total_ttc = -105.24;
        $expected->bases = array(
            70000001 => -100,
            70000002 => 0,
            70000003 => 0,
        );
        $expected->iva = array(
            477000021 => array(
                'cuenta_base' => '',
                'tx' => 21,
                'retx' => 0,
                'bi' => -100,
                'amount' => -21,
                'reamount' => 0
            ),
            477000010 => array(
                'cuenta_base' => '70000003',
                'tx' => 10,
                'retx' => 0,
                'bi' => 0,
                'amount' => 0,
                'reamount' => 0
            ),
        );
        $expected->localtax1 = array(
            477002152 => -5.2,
            477001004 => 0
        );
        $expected->localtax2 = array(
            475100021 => -21,
            475100010 => 0
        );

        $this->assertEquals($expected, EnlaceKeme::restarFactura($factura2, $factura1));

        // Segundo caso: que en la 2º factura haya más bases de IVA, RE e IRPF que en la 1º

        /**
         * Retratamos la siguiente factura:
         *
         * Venta producto A (70000001) => 300 €
         * Venta producto B (70000002) => 400 €
         * Venta producto C (70000003) => 10 €
         * Venta producto D (70000004) => 50 €
         * Total IVA:
         * - 21 % (A+B) (477000021) => 147
         * - 10 % (C) (477000010) => 1
         * - 4 % (D) (477000004) => 2
         * Total RE:
         * - 5,2 % (A+B) (477002152) => 36,4
         * - 0,4 % (C) (477001004) => 0,04
         * - 0,1 % (D) (477000401) => 0.05
         * Total IRPF:
         * - 21 % (475100021) => 147
         * - 10 % (C) (475100010) => 1
         * - 5 % (D) (475100004) => 2.5
         *
         * Total factura: 746.4 €
         */

        $factura2 = new InvoiceDescriptor();
        $factura2->date = '20140101010101';
        $factura2->ref = 'FA2014/0001';
        $factura2->type = InvoiceDescriptor::TYPE_GENERAL;
        $factura2->total_ht = 760;
        $factura2->total_ttc = 795.99;
        $factura2->bases = array(
            70000001 => 300,
            70000002 => 400,
            70000003 => 10,
            70000004 => 50
        );
        $factura2->iva = array(
            477000021 => array(
                'cuenta_base' => '',
                'tx' => 21,
                'retx' => 0,
                'bi' => 700,
                'amount' => 147,
                'reamount' => 0
            ),
            477000010 => array(
                'cuenta_base' => '70000003',
                'tx' => 10,
                'retx' => 0,
                'bi' => 10,
                'amount' => 1,
                'reamount' => 0
            ),
            477000004 => array(
                'cuenta_base' => '70000004',
                'tx' => 4,
                'retx' => 0,
                'bi' => 50,
                'amount' => 2,
                'reamount' => 0
            ),
        );
        $factura2->localtax1 = array(
            477002152 => 36.4,
            477001004 => 0.4,
            477000401 => 0.05
        );
        $factura2->localtax2 = array(
            475100021 => 147,
            475100010 => 1,
            475100004 => 2.5
        );

        $expected = new InvoiceDescriptor();
        $expected->total_ht = 50;
        $expected->total_ttc = 55.65;
        $expected->bases = array(
            70000001 => 100,
            70000002 => 0,
            70000003 => 0,
            70000004 => 50
        );
        $expected->iva = array(
            477000021 => array(
                'cuenta_base' => '',
                'tx' => 21,
                'retx' => 0,
                'bi' => 100,
                'amount' => 21,
                'reamount' => 0
            ),
            477000010 => array(
                'cuenta_base' => '70000003',
                'tx' => 10,
                'retx' => 0,
                'bi' => 0,
                'amount' => 0,
                'reamount' => 0
            ),
            477000004 => array(
                'cuenta_base' => '70000004',
                'tx' => 4,
                'retx' => 0,
                'bi' => 50,
                'amount' => 2,
                'reamount' => 0
            ),
        );
        $expected->localtax1 = array(
            477002152 => 5.2,
            477001004 => 0,
            477000401 => 0.05
        );
        $expected->localtax2 = array(
            475100021 => 21,
            475100010 => 0,
            475100004 => 2.5
        );

        $this->assertEquals($expected, EnlaceKeme::restarFactura($factura1, $factura2));
    }

    /**
     * @covers Marcosgdf\EnlaceKeme\EnlaceKeme::processPayments
     * @covers Marcosgdf\EnlaceKeme\EnlaceKeme::createAsiento
     */
    public function testProcessPaymentsCustomer()
    {
        /**
         * Cobro de factura
         */

        $qresult = (object)array(
            'datep' => '2015-01-05 12:00:00',
            'amount' => 21.75,
            'account_number' => '123456',
            'facnumber' => 'FA1',
            'code_compta' => '987654'
        );

        $expected_concepto = 'Cobro de la factura ' . $qresult->facnumber;

        $expected_diario = new CommonBag("{diario}");

        //Debe
        $apunte = new ApunteFormat();
        $apunte->numero = 1;
        $apunte->apunte = 1;
        $apunte->fecha = new DateTime('05-01-2015');
        $apunte->cuenta = $qresult->account_number;
        $apunte->concepto = $expected_concepto;
        $apunte->debe = $qresult->amount;
        $apunte->documento = $qresult->facnumber;

        $expected_diario->add($apunte);

        //Haber
        $apunte = new ApunteFormat();
        $apunte->numero = 1;
        $apunte->apunte = 2;
        $apunte->fecha = new DateTime('05-01-2015');
        $apunte->cuenta = $qresult->code_compta;
        $apunte->concepto = $expected_concepto;
        $apunte->haber = $qresult->amount;
        $apunte->documento = $qresult->facnumber;

        $expected_diario->add($apunte);

        $reflection = new ReflectionMethod('Marcosgdf\EnlaceKeme\EnlaceKeme', 'processPayments');
        $reflection->setAccessible(true);

        $reflection->invoke(self::$ek, $qresult);

        $this->assertEquals($expected_diario, self::$ek->return_diario);

        /**
         * Pago de factura (rectificativa, sale dinero de banco)
         */

        $qresult->amount = -$qresult->amount;

        $expected_concepto = 'Pago de la factura ' . $qresult->facnumber;

        //Debe
        $apunte = new ApunteFormat();
        $apunte->numero = 2;
        $apunte->apunte = 3;
        $apunte->fecha = new DateTime('05-01-2015');
        $apunte->cuenta = $qresult->account_number;
        $apunte->concepto = $expected_concepto;
        $apunte->haber = abs($qresult->amount);
        $apunte->documento = $qresult->facnumber;

        $expected_diario->add($apunte);

        //Haber
        $apunte = new ApunteFormat();
        $apunte->numero = 2;
        $apunte->apunte = 4;
        $apunte->fecha = new DateTime('05-01-2015');
        $apunte->cuenta = $qresult->code_compta;
        $apunte->concepto = $expected_concepto;
        $apunte->debe = abs($qresult->amount);
        $apunte->documento = $qresult->facnumber;

        $expected_diario->add($apunte);

        $reflection->invoke(self::$ek, $qresult);

        $this->assertEquals($expected_diario, self::$ek->return_diario);
    }

    /**
     * @covers Marcosgdf\EnlaceKeme\EnlaceKeme::processPayments
     * @covers Marcosgdf\EnlaceKeme\EnlaceKeme::createAsiento
     */
    public function testProcessPaymentsSupplier()
    {
        /**
         * Cobro de factura
         */

        $qresult = (object)array(
            'datep' => '2015-01-05 12:00:00',
            'amount' => 21.75,
            'account_number' => '123456',
            'facnumber' => 'FA1',
            'code_compta' => '987654'
        );

        $expected_concepto = 'Pago de la factura ' . $qresult->facnumber;

        $expected_diario = new CommonBag("{diario}");

        //Debe
        $apunte = new ApunteFormat();
        $apunte->numero = 1;
        $apunte->apunte = 1;
        $apunte->fecha = new DateTime('05-01-2015');
        $apunte->cuenta = $qresult->account_number;
        $apunte->concepto = $expected_concepto;
        $apunte->debe = $qresult->amount;
        $apunte->documento = $qresult->facnumber;

        $expected_diario->add($apunte);

        //Haber
        $apunte = new ApunteFormat();
        $apunte->numero = 1;
        $apunte->apunte = 2;
        $apunte->fecha = new DateTime('05-01-2015');
        $apunte->cuenta = $qresult->code_compta;
        $apunte->concepto = $expected_concepto;
        $apunte->haber = $qresult->amount;
        $apunte->documento = $qresult->facnumber;

        $expected_diario->add($apunte);

        $reflection = new ReflectionMethod('Marcosgdf\EnlaceKeme\EnlaceKeme', 'processPayments');
        $reflection->setAccessible(true);

        $reflection->invoke(self::$ek, $qresult, true);

        $this->assertEquals($expected_diario, self::$ek->return_diario);

        /**
         * Pago de factura (rectificativa, sale dinero de banco)
         */

        $qresult->amount = -$qresult->amount;

        $expected_concepto = 'Cobro de la factura ' . $qresult->facnumber;

        //Debe
        $apunte = new ApunteFormat();
        $apunte->numero = 2;
        $apunte->apunte = 3;
        $apunte->fecha = new DateTime('05-01-2015');
        $apunte->cuenta = $qresult->account_number;
        $apunte->concepto = $expected_concepto;
        $apunte->haber = abs($qresult->amount);
        $apunte->documento = $qresult->facnumber;

        $expected_diario->add($apunte);

        //Haber
        $apunte = new ApunteFormat();
        $apunte->numero = 2;
        $apunte->apunte = 4;
        $apunte->fecha = new DateTime('05-01-2015');
        $apunte->cuenta = $qresult->code_compta;
        $apunte->concepto = $expected_concepto;
        $apunte->debe = abs($qresult->amount);
        $apunte->documento = $qresult->facnumber;

        $expected_diario->add($apunte);

        $reflection->invoke(self::$ek, $qresult, true);

        $this->assertEquals($expected_diario, self::$ek->return_diario);
    }

    /**
     * @covers Marcosgdf\EnlaceKeme\EnlaceKeme::processThirds
     */
    public function testProcessThirds()
    {
        $qresult = (object)array(
            'name' => 'Empresa de prueba, S.L.',
            'zip' => '15011',
            'town' => 'A Coruña',
            'code_compta' => '134566',
            'code_compta_fournisseur' => '234567',
            'siren' => '000000014Z',
            'url' => 'http://www.marcosgdf.com',
            'address' => 'Calle de prueba, 2',
            'phone' => '0034590138912',
            'email' => 'hola@marcosgdf.com',
            'fax' => '00345837281',
            'country_code' => 'ES',
            'state' => 'A Coruña'
        );

        $expected_plancontable = new CommonBag("{plancontable}");

        $plancontable = new \Marcosgdf\EnlaceKeme\Format\PlancontableFormat();
        $plancontable->cuenta = $qresult->code_compta;
        $plancontable->nombre = $qresult->name;

        $expected_plancontable->add($plancontable);

        $expected_da = new CommonBag("{datos_auxiliares}");

        $fill_datos = function ($qresult, $supplier, CommonBag &$expected_da) {

            $datosauxiliares = new \Marcosgdf\EnlaceKeme\Format\DatosauxiliaresFormat();

            if (!$supplier) {
                $datosauxiliares->cuenta = $qresult->code_compta;
            } else {
                $datosauxiliares->cuenta = $qresult->code_compta_fournisseur;
            }

            $datosauxiliares->razon_social = $qresult->name;
            $datosauxiliares->nif = $qresult->siren;
            $datosauxiliares->web = $qresult->url;
            $datosauxiliares->domicilio = str_replace("\r\n", ", ", $qresult->address);
            $datosauxiliares->poblacion = $qresult->town;
            $datosauxiliares->cp = $qresult->zip;
            $datosauxiliares->provincia = $qresult->state;
            $datosauxiliares->telefono = $qresult->phone;
            $datosauxiliares->pais = 'ES';
            $datosauxiliares->fax = $qresult->fax;
            $datosauxiliares->email = $qresult->email;

            $expected_da->add($datosauxiliares);
        };

        $fill_datos($qresult, false, $expected_da);

        $reflection = new ReflectionMethod('Marcosgdf\EnlaceKeme\EnlaceKeme', 'processThirds');
        $reflection->setAccessible(true);

        $reflection->invoke(self::$ek, EnlaceKeme::EXPORT_CUSTOMERS, $qresult);

        $this->assertEquals($expected_plancontable, self::$ek->return_plancontable);
        $this->assertEquals($expected_da, self::$ek->return_datosauxiliares);

        $reflection->invoke(self::$ek, EnlaceKeme::EXPORT_SUPPLIERS, $qresult);

        $plancontable = new \Marcosgdf\EnlaceKeme\Format\PlancontableFormat();
        $plancontable->cuenta = $qresult->code_compta_fournisseur;
        $plancontable->nombre = $qresult->name;

        $expected_plancontable->add($plancontable);

        $fill_datos($qresult, true, $expected_da);

        $this->assertEquals($expected_plancontable, self::$ek->return_plancontable);
        $this->assertEquals($expected_da, self::$ek->return_datosauxiliares);
    }

    /**
     * @covers Marcosgdf\EnlaceKeme\EnlaceKeme::processInvoiceQueryResult
     */
    public function testProcessInvoiceQueryResultFail()
    {
        $sql = file_get_contents(__DIR__.'/src/customer_invoices.js');

        $json_query_result = json_decode($sql, true);

        $tabttc = array();
        $tabfac = array();
        $expected_tabttc = array();
        $expected_tabfac = array();

        $reflection = new ReflectionMethod('Marcosgdf\EnlaceKeme\EnlaceKeme', 'processInvoiceQueryResult');
        $reflection->setAccessible(true);

        //Eliminamos la cuenta de IVA
        $json_query_result[1]['account_tva'] = null;

        $newline = (object)$json_query_result[1];

        $this->setExpectedException('RuntimeException', 'Las cuentas de IVA no están completamente definidas.');

        $reflection->invokeArgs(self::$ek, array(
            $newline,
            true,
            &$tabfac,
            &$tabttc
        ));
    }

    /**
     * @covers Marcosgdf\EnlaceKeme\EnlaceKeme::processInvoiceQueryResult
     */
    public function testProcessInvoiceQueryResult()
    {
        global $db;

        $sql = file_get_contents(__DIR__.'/src/customer_invoices.js');
        $jsql = file_get_contents(__DIR__.'/src/customer_invoices_eu.js');

        $json_query_result = json_decode($sql, true);
        $json_jquery_result = json_decode($jsql, true);

        $tabttc = array();
        $tabfac = array();
        $expected_tabttc = array();
        $expected_tabfac = array();

        $reflection = new ReflectionMethod('Marcosgdf\EnlaceKeme\EnlaceKeme', 'processInvoiceQueryResult');
        $reflection->setAccessible(true);

        foreach ($json_query_result as $line) {

            $newline = (object)$line;

            $db::$num_rows = $json_jquery_result[$newline->rowid]['num_rows'];
            $db::$result = $json_jquery_result[$newline->rowid]['result'];

            $reflection->invokeArgs(self::$ek, array(
                $newline,
                true,
                &$tabfac,
                &$tabttc
            ));
        }

        require_once 'invoices/Customer/Invoice1Test.php';
        require_once 'invoices/Customer/Invoice2Test.php';
        require_once 'invoices/Customer/Invoice3Test.php';
        require_once 'invoices/Customer/Invoice4Test.php';
        require_once 'invoices/Customer/Invoice5Test.php';
        require_once 'invoices/Customer/Invoice6Test.php';
        require_once 'invoices/Customer/Invoice7Test.php';
        require_once 'invoices/Customer/Invoice8Test.php';
        require_once 'invoices/Customer/Invoice9Test.php';

        $invoicesToTest = array(
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice1Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice2Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice3Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice4Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice5Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice6Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice7Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice8Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice9Test()
        );

        foreach ($invoicesToTest as $invoice) {
            $expected_tabfac[$invoice->getId()] = $invoice->getDescriptor(self::$ek->cache_re);
            $expected_tabttc[$invoice->getId()] = $invoice->getContrapartidas();
        }

        $this->assertEquals($expected_tabfac, $tabfac);
        $this->assertEquals($expected_tabttc, $tabttc);

        /**
         * Facturas de proveedores
         */

        $sql = file_get_contents(__DIR__.'/src/supplier_invoices.js');
        $jsql = file_get_contents(__DIR__.'/src/supplier_invoices_eu.js');

        $json_query_result = json_decode($sql, true);
        $json_jquery_result = json_decode($jsql, true);

        $tabfac = array();
        $tabttc = array();

        foreach ($json_query_result as $line) {

            $newline = (object)$line;

            $db::$num_rows = $json_jquery_result[$newline->rowid]['num_rows'];
            $db::$result = $json_jquery_result[$newline->rowid]['result'];

            $reflection->invokeArgs(self::$ek, array(
                $newline,
                false,
                &$tabfac,
                &$tabttc
            ));
        }

        require_once 'invoices/Supplier/Invoice1Test.php';
        require_once 'invoices/Supplier/Invoice2Test.php';
        require_once 'invoices/Supplier/Invoice3Test.php';
        require_once 'invoices/Supplier/Invoice4Test.php';
        require_once 'invoices/Supplier/Invoice5Test.php';
        require_once 'invoices/Supplier/Invoice6Test.php';
        require_once 'invoices/Supplier/Invoice7Test.php';

        $expected_tabfac = array();
        $expected_tabttc = array();

        $invoicesToTest = array(
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier\Invoice1Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier\Invoice2Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier\Invoice3Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier\Invoice4Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier\Invoice5Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier\Invoice6Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier\Invoice7Test(),
        );

        foreach ($invoicesToTest as $invoice) {
            $expected_tabfac[$invoice->getId()] = $invoice->getDescriptor(self::$ek->cache_re);
            $expected_tabttc[$invoice->getId()] = $invoice->getContrapartidas();
        }

        $this->assertEquals($expected_tabfac, $tabfac);
        $this->assertEquals($expected_tabttc, $tabttc);
    }

    /**
     * @covers Marcosgdf\EnlaceKeme\EnlaceKeme::obtenerCuentaFromIva
     */
    public function testObtenerCuentaFromIva()
    {
        $reflection = new ReflectionMethod('Marcosgdf\EnlaceKeme\EnlaceKeme', 'obtenerCuentaFromIva');
        $reflection->setAccessible(true);

        foreach (self::$ek->cache_re as $tasa => $cuentas) {
            $this->assertEquals(self::$ek->cache_re[$tasa]['buy'], $reflection->invoke(self::$ek, $tasa));
            $this->assertEquals(self::$ek->cache_re[$tasa]['sell'], $reflection->invoke(self::$ek, $tasa, true));
        }
    }

    /**
     * @covers Marcosgdf\EnlaceKeme\EnlaceKeme::processTaxPayments
     */
    public function testProcessTaxPayments()
    {
        $qresult = (object) array(
            'account_number' => '57200001',
            'label' => 'IVA 4T',
            'amount' => 220.35,
            'datep' => '2015-01-01'
        );

        $reflection = new ReflectionMethod('Marcosgdf\EnlaceKeme\EnlaceKeme', 'processTaxPayments');
        $reflection->setAccessible(true);

        $reflection->invoke(self::$ek, $qresult, 123456789);

        $expected_diario = new CommonBag("{diario}");

        $expected_concepto = $qresult->label;

        //Haber
        $apunte = new ApunteFormat();
        $apunte->numero = 1;
        $apunte->apunte = 1;
        $apunte->fecha = new DateTime('01-01-2015');
        $apunte->cuenta = $qresult->account_number;
        $apunte->concepto = $expected_concepto;
        $apunte->haber = $qresult->amount;

        $expected_diario->add($apunte);

        //Debe
        $apunte = new ApunteFormat();
        $apunte->numero = 1;
        $apunte->apunte = 2;
        $apunte->fecha = new DateTime('01-01-2015');
        $apunte->cuenta = '123456789';
        $apunte->concepto = $expected_concepto;
        $apunte->debe = $qresult->amount;

        $expected_diario->add($apunte);

        $this->assertEquals($expected_diario, self::$ek->return_diario);
    }

    /**
     * @covers Marcosgdf\EnlaceKeme\EnlaceKeme::processInvoices
     */
    public function testProcessInvoicesSupplier()
    {
        global $db;

        $sql = file_get_contents(__DIR__.'/src/supplier_invoices.js');
        $jsql = file_get_contents(__DIR__.'/src/supplier_invoices_eu.js');

        $json_query_result = json_decode($sql, true);
        $json_jquery_result = json_decode($jsql, true);

        $tabttc = array();
        $tabfac = array();

        $reflection = new ReflectionMethod('Marcosgdf\EnlaceKeme\EnlaceKeme', 'processInvoiceQueryResult');
        $reflection->setAccessible(true);

        foreach ($json_query_result as $line) {

            $newline = (object)$line;

            $db::$num_rows = $json_jquery_result[$newline->rowid]['num_rows'];
            $db::$result = $json_jquery_result[$newline->rowid]['result'];

            $reflection->invokeArgs(self::$ek, array(
                $newline,
                false,
                &$tabfac,
                &$tabttc
            ));
        }

        $reflection = new ReflectionMethod('Marcosgdf\EnlaceKeme\EnlaceKeme', 'processInvoices');
        $reflection->setAccessible(true);

        $reflection->invoke(self::$ek, $tabfac, $tabttc, true);

        //Lo usaremos para comparar los diarios de $nuevo_ek y self::$ek
        $nuevo_ek = clone self::$ek;
        $nuevo_ek->return_diario = new CommonBag('{diario}');
        $nuevo_ek->apunte_counter = 1;
        $nuevo_ek->asiento_counter = 1;

        $expected_lf = new CommonBag("{libro_facturas}");

        $reflection = new ReflectionMethod('Marcosgdf\EnlaceKeme\EnlaceKeme', 'createAsiento');
        $reflection->setAccessible(true);

        require_once 'invoices/Supplier/Invoice1Test.php';
        require_once 'invoices/Supplier/Invoice2Test.php';
        require_once 'invoices/Supplier/Invoice3Test.php';
        require_once 'invoices/Supplier/Invoice4Test.php';
        require_once 'invoices/Supplier/Invoice5Test.php';
        require_once 'invoices/Supplier/Invoice6Test.php';
        require_once 'invoices/Supplier/Invoice7Test.php';

        $invoicesToTest = array(
            new Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier\Invoice1Test(),
            new Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier\Invoice2Test(),
            new Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier\Invoice3Test(),
            new Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier\Invoice4Test(),
            new Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier\Invoice5Test(),
            new Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier\Invoice6Test(),
            new Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier\Invoice7Test(),
        );

        foreach ($invoicesToTest as $invoice) {
            $reflection->invoke(
                $nuevo_ek,
                $invoice->getDate(),
                $invoice->getConcepto(),
                $invoice->getDocumento(),
                '',
                $invoice->getApuntes()
            );

            $expected_lf.= $invoice->getExpectedFormat();
        }

        $this->assertEquals($nuevo_ek->return_diario, self::$ek->return_diario);
        $this->assertEquals($expected_lf, self::$ek->return_librofacturas);
    }

    /**
     * @covers Marcosgdf\EnlaceKeme\EnlaceKeme::processInvoices
     */
    public function testProcessInvoicesCustomer()
    {
        global $db;

        $sql = file_get_contents(__DIR__.'/src/customer_invoices.js');
        $jsql = file_get_contents(__DIR__.'/src/customer_invoices_eu.js');

        $json_query_result = json_decode($sql, true);
        $json_jquery_result = json_decode($jsql, true);

        $tabttc = array();
        $tabfac = array();

        $reflection = new ReflectionMethod('Marcosgdf\EnlaceKeme\EnlaceKeme', 'processInvoiceQueryResult');
        $reflection->setAccessible(true);

        foreach ($json_query_result as $line) {

            $newline = (object)$line;

            $db::$num_rows = $json_jquery_result[$newline->rowid]['num_rows'];
            $db::$result = $json_jquery_result[$newline->rowid]['result'];

            $reflection->invokeArgs(self::$ek, array(
                $newline,
                true,
                &$tabfac,
                &$tabttc
            ));
        }

        //Trampeamos la clase EK para facilitar el testeo de facturas rectificativas
        $invoice_11 = new InvoiceDescriptor();

        $invoice_11->date = "2015-01-05";
        $invoice_11->ref = "FA1501-0004";
        $invoice_11->type = InvoiceDescriptor::TYPE_GENERAL;
        $invoice_11->total_ht = 100;
        $invoice_11->total_ttc = 121;
        $invoice_11->bases = array(
            70500000 => 200
        );
        $invoice_11->iva = array(
            47700021 => array(
                'cuenta_base' => '70500000',
                'tx' => 21,
                'retx' => 0,
                'bi' => 100,
                'amount' => 21,
                'reamount' => 0
            )
        );
        $invoice_11->localtax1 = array(
            self::$ek->cache_re[21]['sell'] => 0,
        );
        $invoice_11->localtax2 = array(
            47510000 => 0,
        );

        self::$ek->cache_facturas = array(
            11 => $invoice_11
        );

        $reflection = new ReflectionMethod('Marcosgdf\EnlaceKeme\EnlaceKeme', 'processInvoices');
        $reflection->setAccessible(true);

        $reflection->invoke(self::$ek, $tabfac, $tabttc, false);

        //Lo usaremos para comparar los diarios de $nuevo_ek y self::$ek
        $nuevo_ek = clone self::$ek;
        $nuevo_ek->return_diario = new CommonBag('{diario}');
        $nuevo_ek->apunte_counter = 1;
        $nuevo_ek->asiento_counter = 1;

        $expected_lf = new CommonBag("{libro_facturas}");

        $reflection = new ReflectionMethod('Marcosgdf\EnlaceKeme\EnlaceKeme', 'createAsiento');
        $reflection->setAccessible(true);

        require_once 'invoices/Customer/Invoice1Test.php';
        require_once 'invoices/Customer/Invoice2Test.php';
        require_once 'invoices/Customer/Invoice3Test.php';
        require_once 'invoices/Customer/Invoice4Test.php';
        require_once 'invoices/Customer/Invoice5Test.php';
        require_once 'invoices/Customer/Invoice6Test.php';
        require_once 'invoices/Customer/Invoice7Test.php';
        require_once 'invoices/Customer/Invoice8Test.php';
        require_once 'invoices/Customer/Invoice9Test.php';

        $invoicesToTest = array(
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice1Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice2Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice3Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice4Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice5Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice6Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice7Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice8Test(),
            new \Marcosgdf\EnlaceKeme\Tests\Invoices\Customer\Invoice9Test(),
        );

        foreach ($invoicesToTest as $invoice) {
            $reflection->invoke(
                $nuevo_ek,
                $invoice->getDate(),
                $invoice->getConcepto(),
                $invoice->getDocumento(),
                '',
                $invoice->getApuntes()
            );

            $expected_lf.= $invoice->getExpectedFormat();
        }

        $this->assertEquals($nuevo_ek->return_diario, self::$ek->return_diario);
        $this->assertEquals($expected_lf, self::$ek->return_librofacturas);
    }

    /**
     * @covers Marcosgdf\EnlaceKeme\EnlaceKeme::obtenerRelacionIvaFromTx
     */
    public function testObtenerRelacionIvaFromTx()
    {
        $gn = array(
            'ref' => 'GN',
            'ref_re' => 'GNR'
        );
        $rd = array(
            'ref' => 'RD',
            'ref_re' => 'RD2'
        );
        $srd = array(
            'ref' => 'SRD',
            'ref_re' => 'SRD2'
        );

        self::$ek->cache_kemeref = array(
            21 => $gn,
            10 => $rd,
            4 => $srd,
        );

        $reflection = new ReflectionMethod('Marcosgdf\EnlaceKeme\EnlaceKeme', 'obtenerRelacionIvaFromTx');
        $reflection->setAccessible(true);

        $this->assertEquals($gn['ref'], $reflection->invoke(self::$ek, 21));
        $this->assertEquals($gn['ref_re'], $reflection->invoke(self::$ek, 21, true));
        $this->assertEquals($rd['ref'], $reflection->invoke(self::$ek, 10));
        $this->assertEquals($rd['ref_re'], $reflection->invoke(self::$ek, 10, true));
        $this->assertEquals($srd['ref'], $reflection->invoke(self::$ek, 4));
        $this->assertEquals($srd['ref_re'], $reflection->invoke(self::$ek, 4, true));
    }
}
