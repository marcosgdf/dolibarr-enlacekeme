<?php

require_once __DIR__.'/../enlacekeme/class/Apunte.php';

use Marcosgdf\EnlaceKeme\Apunte;

class ApunteTest extends PHPUnit_Framework_TestCase
{

	public function testGetAmount()
	{
		$apunte = new Apunte();

		//Convierte 1.23 a 1.23
		$apunte->amount = 1.23;
		$this->assertEquals('1.23', $apunte->getAmount());

		//Convierte 1.2 a 1.20
		$apunte->amount = '1.2';
		$this->assertEquals('1.2', $apunte->getAmount());

		//No pone , en los miles
		$apunte->amount = 1000.2;
		$this->assertEquals('1000.20', $apunte->getAmount());
	}

	public function testCalcType()
	{
		$apunte = new Apunte();

		$apunte->amount = 1;
		$apunte->calcType();
		$this->assertEquals(Apunte::TYPE_HABER, $apunte->type);

		$apunte->amount = -1;
		$apunte->calcType();
		$this->assertEquals(Apunte::TYPE_DEBE, $apunte->type);

		//Prueba de inversión de resultado
		$apunte->amount = 1;
		$apunte->calcType(true);
		$this->assertEquals(Apunte::TYPE_DEBE, $apunte->type);

		$apunte->amount = -1;
		$apunte->calcType(true);
		$this->assertEquals(Apunte::TYPE_HABER, $apunte->type);
	}

}