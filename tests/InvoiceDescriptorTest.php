<?php

namespace Marcosgdf\EnlaceKeme\Tests;

use Marcosgdf\EnlaceKeme\InvoiceDescriptor;

class InvoiceDescriptorTest extends \PHPUnit_Framework_TestCase
{

    public static function setUpBeforeClass()
    {
        require_once __DIR__.'/../enlacekeme/class/InvoiceDescriptor.php';
    }

    /**
     * @covers Marcosgdf\EnlaceKeme\InvoiceDescriptor::invert
     */
    public function testAbs()
    {
        $test = new InvoiceDescriptor();
        $test->iva = array(
            'pos' => 1,
            'neg' => -1
        );

        $expected = new InvoiceDescriptor();
        $expected->iva = array(
            'pos' => -1,
            'neg' => 1
        );

        $test->invert();

        $this->assertEquals($expected, $test);

        $test->iva = array(
            array(
                'pos' => 1,
                'neg' => -1
            ),
            array(
                'pos' => -1,
                'neg' => 1
            )
        );

        $expected->iva = array(
            array(
                'pos' => -1,
                'neg' => 1
            ),
            array(
                'pos' => 1,
                'neg' => -1
            ),
        );

        $test->invert();

        $this->assertEquals($expected, $test);

        //Booleans should not be converted
        $test->iva = array(
            array(
                false
            ),
            array(
                true
            )
        );

        $expected = $test;

        $test->invert();

        $this->assertEquals($expected, $test);
    }
}