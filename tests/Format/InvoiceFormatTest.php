<?php

namespace Marcosgdf\EnlaceKeme\Tests\Link;

use Marcosgdf\EnlaceKeme\Format\InvoiceFormat;

class InvoiceFormatTest extends \PHPUnit_Framework_TestCase
{
    public function testToString()
    {
        $factura = new InvoiceFormat();
        $apunte = 342;
        $factura->apunte = $apunte;
        $cuenta_iva = '47200001';
        $factura->cuenta_iva = $cuenta_iva;
        $bi = 200.20;
        $factura->bi = $bi;
        $iva_clave = 'GN';
        $factura->iva_clave = $iva_clave;
        $iva_tx = 21;
        $factura->iva_tx = $iva_tx;
        $re_tx = 0.20;
        $factura->re_tx = $re_tx;
        $iva_amount = 12.90;
        $factura->iva_amount = $iva_amount;
        $re_amount = 12.2;
        $factura->re_amount = $re_amount;
        $cuenta_factura = '4300001';
        $factura->cuenta_factura = $cuenta_factura;
        $fecha = new \DateTime('now');
        $factura->fecha_factura = $fecha;
        $factura->soportado = false;
        $factura->aib = false;
        $factura->ais = true;
        $factura->ase = false;
        $factura->pis = false;
        $factura->inversion = true;
        $factura->eib = false;
        $factura->fechaop = $fecha;
        $factura->exportacion = true;

        //Apunte en el diario de la compra
        $expected = str_pad($apunte, 15, '0', STR_PAD_LEFT);
        //Cuenta base IVA
        $expected .= str_pad($cuenta_iva, 30, ' ');
        //Base imponible
        $expected .= str_pad(number_format($bi, 2, '.', ''), 17, '0', STR_PAD_LEFT);
        //Clave IVA
        $expected .= str_pad($iva_clave, 15, ' ');
        //Tipo IVA (%)
        $expected .= str_pad(number_format($iva_tx, 2, '.', ''), 5, '0', STR_PAD_LEFT);
        //Tipo Recargo equiv. (%)
        $expected .= str_pad(number_format($re_tx, 2, '.', ''), 5, '0', STR_PAD_LEFT);
        //Cuota IVA
        $expected .= str_pad(number_format($iva_amount, 2, '.', ''), 17, '0', STR_PAD_LEFT);
        //Cuota REC
        $expected .= str_pad(number_format($re_amount, 2, '.', ''), 17, '0', STR_PAD_LEFT);
        //Cuenta de factura
        $expected .= str_pad($cuenta_factura, 30, ' ');
        //Fecha de factura
        $expected .= $fecha->format('d-m-Y');
        //¿Es soportado?
        $expected .= 0;
        //¿Es AIB?
        $expected .= 0;
        //¿Es AIS?
        $expected .= 1;
        //¿Es AS no UE?
        $expected .= 0;
        //¿Es PIS?
        $expected .= 0;
        //¿Bien de inversión?
        $expected .= 1;
        //¿Es EIB?
        $expected .= 0;
        //Fecha operación
        $expected .= $fecha->format('d-m-Y');
        //Exportación
        $expected .= 1;

        $this->assertEquals($expected, (string) $factura);
    }
}