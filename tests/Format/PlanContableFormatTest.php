<?php

namespace Marcosgdf\EnlaceKeme\Tests\Link;

use Marcosgdf\EnlaceKeme\Format\PlancontableFormat;

class PlanContableFormatTest extends \PHPUnit_Framework_TestCase
{
    public function testToString()
    {
        $plancontable = new PlancontableFormat();
        $plancontable->cuenta = '123';
        $plancontable->nombre = 'ABC';

        //Número de cuenta
        $expected = str_pad('123', 30, ' ');
        //Nombre de la cuenta
        $expected .= str_pad('ABC', 80, ' ');

        $this->assertEquals($expected, (string) $plancontable);
    }
}