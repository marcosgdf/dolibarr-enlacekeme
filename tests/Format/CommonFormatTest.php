<?php

namespace Marcosgdf\EnlaceKeme\Tests\Link;

use Marcosgdf\EnlaceKeme\Format\CommonFormat;

class CommonFormatTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers Marcosgdf\EnlaceKeme\CommonFormat::fill
     */
    public function testFill()
    {
        //Prueba con floats
        $float = 2.1;
        $expected = '02.10';
        $this->assertEquals($expected, CommonFormat::fill($float, 4));

        //Prueba con integers
        $int = 10;
        $expected = '0010';
        $this->assertEquals($expected, CommonFormat::fill($int, 4));

        //Prueba con strings
        $string = 'A Coruña';
        $expected = 'A Coruña    ';
        $this->assertEquals($expected, CommonFormat::fill($string, 12));

        //Prueba de excepción
        $string = new \stdClass();
        $this->setExpectedException('InvalidArgumentException', 'El valor de $string no es int, float o string');
        CommonFormat::fill($string, 2);
    }
}