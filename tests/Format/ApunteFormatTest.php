<?php

namespace Marcosgdf\EnlaceKeme\Tests\Link;

use Marcosgdf\EnlaceKeme\Format\ApunteFormat;

class ApunteFormatTest extends \PHPUnit_Framework_TestCase
{

    public function testToString()
    {
        $numero = 10;
        $apuntenum = 40;
        $diario = 'E';
        $fecha = new \DateTime('now');
        $cuenta = '523';
        $concepto = 'Concepto de prueba';
        $debe = 10;
        $documento = 'DOCUMENTO';
        $codfactura = 10;

        $apunte = new ApunteFormat();
        $apunte->numero = $numero;
        $apunte->apunte = $apuntenum;
        $apunte->diario = $diario;
        $apunte->fecha = $fecha;
        $apunte->cuenta = $cuenta;
        $apunte->concepto = $concepto;
        $apunte->debe = $debe;
        $apunte->documento = $documento;
        $apunte->codfactura = $codfactura;

        //Número
        $expected = str_pad($numero, 15, '0', STR_PAD_LEFT);
        //Apunte
        $expected .= str_pad($apuntenum, 15, '0', STR_PAD_LEFT);
        //Diario
        $expected .= str_pad($diario, 40, ' ');
        //Fecha
        $expected .= $fecha->format('d-m-Y');
        //Código cuenta
        $expected .= str_pad($cuenta, 30, ' ');
        //Concepto cargo/abono
        $expected .= str_pad($concepto, 100, ' ');
        //Debe
        $expected .= str_pad(number_format($debe, 2, '.', ''), 17, '0', STR_PAD_LEFT);
        //Haber
        $expected .= str_pad(number_format('0', 2, '.', ''), 17, '0', STR_PAD_LEFT);
        //Documento
        $expected .= str_pad($documento, 80, ' ');
        //Cód. Factura
        $expected .= str_pad($codfactura, 80, ' ');

        $this->assertEquals($expected, (string)$apunte);

        //Comprobación de que no se puede definir el debe y haber a la vez
        $this->setExpectedException('RuntimeException', 'El debe y el haber no pueden estar definidos a la vez');

        $apunte->haber = 1;
        $apunte->__toString();
    }
}