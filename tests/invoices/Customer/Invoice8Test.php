<?php

namespace Marcosgdf\EnlaceKeme\Tests\Invoices\Customer;

require_once __DIR__.'/../../../enlacekeme/class/Apunte.php';
require_once __DIR__.'/../../TestInterfaces/InvoiceTestInterface.php';

use Marcosgdf\EnlaceKeme\InvoiceDescriptor;
use Marcosgdf\EnlaceKeme\Format\InvoiceFormat;
use Marcosgdf\EnlaceKeme\Tests\TestInterfaces\InvoiceTestInterface;
use Marcosgdf\EnlaceKeme\Apunte;

/**
 * Factura rectificativa
 */
class Invoice8Test implements InvoiceTestInterface
{
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 16;
    }

    /**
     * {@inheritdoc}
     */
    public function getDate()
    {
        return new \DateTime('08-01-2015');
    }

    /**
     * {@inheritdoc}
     */
    public function getConcepto()
    {
        return 'Factura rectificativa FA1501-0006';
    }

    /**
     * {@inheritdoc}
     */
    public function getDocumento()
    {
        return 'FA1501-0006';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescriptor(array $cache_re)
    {
        $descriptor = new InvoiceDescriptor();

        $descriptor->date = "2015-01-08";
        $descriptor->ref = "FA1501-0006";
        $descriptor->type = InvoiceDescriptor::TYPE_RECTIFICATIVA;
        $descriptor->total_ht = 1900;
        $descriptor->total_ttc = 2299;
        $descriptor->bases = array(
            70500000 => 2000
        );
        $descriptor->iva = array(
            47700021 => array(
                'cuenta_base' => '70500000',
                'tx' => 21,
                'retx' => 0,
                'bi' => 1900,
                'amount' => 399,
                'reamount' => 0
            )
        );
        $descriptor->localtax1 = array(
            $cache_re[21]['sell'] => 0,
        );
        $descriptor->localtax2 = array(
            47510000 => 0,
        );
        $descriptor->source = 11;

        return $descriptor;
    }

    /**
     * {@inheritdoc}
     */
    public function getContrapartidas()
    {
        return array(
            12300001 => 100,
            43000001 => 2299
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getApuntes()
    {
        $apuntes = array();

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->account = '70500000';
        $apunte->amount = 1800;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->account = '12300001';
        $apunte->amount = 100;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->account = '43000001';
        $apunte->amount = 2178;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->account = '47700021';
        $apunte->amount = 378;

        $apuntes[] = $apunte;

        return $apuntes;
    }

    /**
     * {@inheritdoc}
     */
    public function getExpectedFormat()
    {
        $expected_lf = '';

        // 21 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->apunte = 32;
        $invoiceformat->cuenta_iva = '70500000';
        $invoiceformat->bi = 1800;
        $invoiceformat->iva_clave = 'GN';
        $invoiceformat->iva_tx = 21;
        $invoiceformat->iva_amount = 378;
        $invoiceformat->cuenta_factura = '43000001';
        $invoiceformat->fecha_factura = new \DateTime('08-01-2015');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        return $expected_lf;
    }
}