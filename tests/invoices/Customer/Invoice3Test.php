<?php

namespace Marcosgdf\EnlaceKeme\Tests\Invoices\Customer;

require_once __DIR__.'/../../../enlacekeme/class/Apunte.php';
require_once __DIR__.'/../../TestInterfaces/InvoiceTestInterface.php';

use Marcosgdf\EnlaceKeme\InvoiceDescriptor;
use Marcosgdf\EnlaceKeme\Format\InvoiceFormat;
use Marcosgdf\EnlaceKeme\Tests\TestInterfaces\InvoiceTestInterface;
use Marcosgdf\EnlaceKeme\Apunte;

class Invoice3Test implements InvoiceTestInterface
{
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 8;
    }

    /**
     * {@inheritdoc}
     */
    public function getDate()
    {
        return new \DateTime('05-01-2015');
    }

    /**
     * {@inheritdoc}
     */
    public function getConcepto()
    {
        return 'Factura '.$this->getDocumento();
    }

    /**
     * {@inheritdoc}
     */
    public function getDocumento()
    {
        return 'FA1501-0003';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescriptor(array $cache_re)
    {
        $descriptor = new InvoiceDescriptor();

        $descriptor->date = "2015-01-05";
        $descriptor->ref = "FA1501-0003";
        $descriptor->type = InvoiceDescriptor::TYPE_GENERAL;
        $descriptor->total_ht = 10;
        $descriptor->total_ttc = 8.95;
        $descriptor->bases = array(
            70500000 => 10
        );
        $descriptor->iva = array(
            47700021 => array(
                'cuenta_base' => '70500000',
                'tx' => 21,
                'retx' => 0,
                'bi' => 5,
                'amount' => 1.05,
                'reamount' => 0
            ),
            null => array(
                'cuenta_base' => '70500000',
                'tx' => 0,
                'retx' => 0,
                'bi' => 5,
                'amount' => 0,
                'reamount' => 0
            )
        );
        $descriptor->localtax1 = array(
            $cache_re[21]['sell'] => 0,
            null => 0
        );
        $descriptor->localtax2 = array(
            47510000 => -2.1,
        );

        return $descriptor;
    }

    /**
     * {@inheritdoc}
     */
    public function getContrapartidas()
    {
        return array(
            43000004 => 8.95
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getApuntes()
    {
        $apuntes = array();

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->account = '70500000';
        $apunte->amount = 10;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->account = '47300000';
        $apunte->amount = 2.1;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->account = '43000004';
        $apunte->amount = 8.95;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->account = '47700021';
        $apunte->amount = 1.05;

        $apuntes[] = $apunte;

        return $apuntes;
    }

    /**
     * {@inheritdoc}
     */
    public function getExpectedFormat()
    {
        $expected_lf = '';

        // 21 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->apunte = 15;
        $invoiceformat->cuenta_iva = '70500000';
        $invoiceformat->bi = 5;
        $invoiceformat->iva_clave = 'GN';
        $invoiceformat->iva_tx = 21;
        $invoiceformat->iva_amount = 1.05;
        $invoiceformat->cuenta_factura = '43000004';
        $invoiceformat->fecha_factura = new \DateTime('05-01-2015');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        // 0 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->apunte = 14;
        $invoiceformat->cuenta_iva = '70500000';
        $invoiceformat->bi = 5;
        $invoiceformat->cuenta_factura = '43000004';
        $invoiceformat->fecha_factura = new \DateTime('05-01-2015');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        return $expected_lf;
    }
}