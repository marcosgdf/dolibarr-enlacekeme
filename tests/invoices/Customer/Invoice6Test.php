<?php

namespace Marcosgdf\EnlaceKeme\Tests\Invoices\Customer;

require_once __DIR__.'/../../../enlacekeme/class/Apunte.php';
require_once __DIR__.'/../../TestInterfaces/InvoiceTestInterface.php';

use Marcosgdf\EnlaceKeme\InvoiceDescriptor;
use Marcosgdf\EnlaceKeme\Format\InvoiceFormat;
use Marcosgdf\EnlaceKeme\Tests\TestInterfaces\InvoiceTestInterface;
use Marcosgdf\EnlaceKeme\Apunte;

/**
 * Factura sin IVA
 */
class Invoice6Test implements InvoiceTestInterface
{
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 12;
    }

    /**
     * {@inheritdoc}
     */
    public function getDate()
    {
        return new \DateTime('06-01-2015');
    }

    /**
     * {@inheritdoc}
     */
    public function getConcepto()
    {
        return 'Factura '.$this->getDocumento();
    }

    /**
     * {@inheritdoc}
     */
    public function getDocumento()
    {
        return 'FA1501-0005';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescriptor(array $cache_re)
    {
        $descriptor = new InvoiceDescriptor();

        $descriptor->date = "2015-01-06";
        $descriptor->ref = "FA1501-0005";
        $descriptor->type = InvoiceDescriptor::TYPE_GENERAL;
        $descriptor->total_ht = 5;
        $descriptor->total_ttc = 5;
        $descriptor->bases = array(
            70000000 => 5
        );
        $descriptor->iva = array(
            null => array(
                'cuenta_base' => '70000000',
                'tx' => 0,
                'retx' => 0,
                'bi' => 5,
                'amount' => 0,
                'reamount' => 0
            )
        );
        $descriptor->localtax1 = array(
            null => 0,
        );
        $descriptor->localtax2 = array(
            47510000 => 0,
        );

        return $descriptor;
    }

    /**
     * {@inheritdoc}
     */
    public function getContrapartidas()
    {
        return array(
            43000003 => 5
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getApuntes()
    {
        $apuntes = array();

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->account = '70000000';
        $apunte->amount = 5;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->account = '43000003';
        $apunte->amount = 5;

        $apuntes[] = $apunte;

        return $apuntes;
    }

    /**
     * {@inheritdoc}
     */
    public function getExpectedFormat()
    {
        $expected_lf = '';

        // 0 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->apunte = 24;
        $invoiceformat->cuenta_iva = '70000000';
        $invoiceformat->bi = 5;
        $invoiceformat->cuenta_factura = '43000003';
        $invoiceformat->fecha_factura = new \DateTime('06-01-2015');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        return $expected_lf;
    }
}