<?php

namespace Marcosgdf\EnlaceKeme\Tests\Invoices\Customer;

require_once __DIR__.'/../../../enlacekeme/class/Apunte.php';
require_once __DIR__.'/../../TestInterfaces/InvoiceTestInterface.php';

use Marcosgdf\EnlaceKeme\InvoiceDescriptor;
use Marcosgdf\EnlaceKeme\Format\InvoiceFormat;
use Marcosgdf\EnlaceKeme\Tests\TestInterfaces\InvoiceTestInterface;
use Marcosgdf\EnlaceKeme\Apunte;

class Invoice1Test implements InvoiceTestInterface
{

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 1;
    }

    /**
     * {@inheritdoc}
     */
    public function getDate()
    {
        return new \DateTime('12-10-2014');
    }

    /**
     * {@inheritdoc}
     */
    public function getConcepto()
    {
        return 'Factura '.$this->getDocumento();
    }

    /**
     * {@inheritdoc}
     */
    public function getDocumento()
    {
        return 'FA1410-0001';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescriptor(array $cache_re)
    {
        $descriptor = new InvoiceDescriptor();

        $descriptor->date = "2014-10-12";
        $descriptor->ref = "FA1410-0001";
        $descriptor->type = InvoiceDescriptor::TYPE_GENERAL;
        $descriptor->total_ht = 20;
        $descriptor->total_ttc = 21.75;
        $descriptor->bases = array(
            70000000 => 20
        );
        $descriptor->iva = array(
            47700021 => array(
                'cuenta_base' => '70000000',
                'tx' => 21,
                'retx' => 0.0,
                'bi' => 5,
                'amount' => 1.05,
                'reamount' => 0.0
            ),
            47700010 => array(
                'cuenta_base' => '70000000',
                'tx' => 10,
                'retx' => 0.0,
                'bi' => 5,
                'amount' => 0.5,
                'reamount' => 0.0
            ),
            47700004 => array(
                'cuenta_base' => '70000000',
                'tx' => 4,
                'retx' => 0.0,
                'bi' => 5,
                'amount' => 0.2,
                'reamount' => 0.0
            ),
            null => array(
                'cuenta_base' => '70000000',
                'tx' => 0,
                'retx' => 0.0,
                'bi' => 5,
                'amount' => 0,
                'reamount' => 0
            )
        );
        $descriptor->localtax1 = array(
            $cache_re[21]['sell'] => 0,
            $cache_re[10]['sell'] => 0,
            $cache_re[4]['sell'] => 0,
            null => 0
        );
        $descriptor->localtax2 = array(
            47510000 => 0
        );

        return $descriptor;
    }

    /**
     * {@inheritdoc}
     */
    public function getContrapartidas()
    {
        return array(
            43000001 => 21.75
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getApuntes()
    {
        $apuntes = array();

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->account = '70000000';
        $apunte->amount = 20;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->account = '43000001';
        $apunte->amount = 21.75;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->account = '47700021';
        $apunte->amount = 1.05;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->account = '47700010';
        $apunte->amount = 0.5;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->account = '47700004';
        $apunte->amount = 0.2;

        $apuntes[] = $apunte;

        return $apuntes;
    }

    /**
     * {@inheritdoc}
     */
    public function getExpectedFormat()
    {
        $expected_lf = '';

        // 21 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->apunte = 3;
        $invoiceformat->cuenta_iva = '70000000';
        $invoiceformat->bi = 5;
        $invoiceformat->iva_clave = 'GN';
        $invoiceformat->iva_tx = 21;
        $invoiceformat->iva_amount = 1.05;
        $invoiceformat->cuenta_factura = '43000001';
        $invoiceformat->fecha_factura = new \DateTime('12-10-2014');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        // 10 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->apunte = 4;
        $invoiceformat->cuenta_iva = '70000000';
        $invoiceformat->bi = 5;
        $invoiceformat->iva_clave = 'RD';
        $invoiceformat->iva_tx = 10;
        $invoiceformat->iva_amount = 0.5;
        $invoiceformat->cuenta_factura = '43000001';
        $invoiceformat->fecha_factura = new \DateTime('12-10-2014');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        //4 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->apunte = 5;
        $invoiceformat->cuenta_iva = '70000000';
        $invoiceformat->bi = 5;
        $invoiceformat->iva_clave = 'SRD';
        $invoiceformat->iva_tx = 4;
        $invoiceformat->iva_amount = 0.2;
        $invoiceformat->cuenta_factura = '43000001';
        $invoiceformat->fecha_factura = new \DateTime('12-10-2014');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        //0 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->apunte = 2;
        $invoiceformat->cuenta_iva = '70000000';
        $invoiceformat->bi = 5;
        $invoiceformat->cuenta_factura = '43000001';
        $invoiceformat->fecha_factura = new \DateTime('12-10-2014');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        return $expected_lf;
    }
}