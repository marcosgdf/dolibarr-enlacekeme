<?php

namespace Marcosgdf\EnlaceKeme\Tests\Invoices\Customer;

require_once __DIR__.'/../../../enlacekeme/class/Apunte.php';
require_once __DIR__.'/../../TestInterfaces/InvoiceTestInterface.php';

use Marcosgdf\EnlaceKeme\InvoiceDescriptor;
use Marcosgdf\EnlaceKeme\Format\InvoiceFormat;
use Marcosgdf\EnlaceKeme\Tests\TestInterfaces\InvoiceTestInterface;
use Marcosgdf\EnlaceKeme\Apunte;

/**
 * Factura de abono
 */
class Invoice7Test implements InvoiceTestInterface
{
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 13;
    }

    /**
     * {@inheritdoc}
     */
    public function getDate()
    {
        return new \DateTime('07-01-2015');
    }

    /**
     * {@inheritdoc}
     */
    public function getConcepto()
    {
        return 'Factura rectificativa AV1501-0001';
    }

    /**
     * {@inheritdoc}
     */
    public function getDocumento()
    {
        return 'AV1501-0001';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescriptor(array $cache_re)
    {
        $descriptor = new InvoiceDescriptor();

        $descriptor->date = "2015-01-07";
        $descriptor->ref = "AV1501-0001";
        $descriptor->type = InvoiceDescriptor::TYPE_ABONO;
        $descriptor->total_ht = -5;
        $descriptor->total_ttc = -5;
        $descriptor->bases = array(
            70800000 => -5
        );
        $descriptor->iva = array(
            47700021 => array(
                'cuenta_base' => '70800000',
                'tx' => 21,
                'retx' => 0,
                'bi' => -5,
                'amount' => -1.05,
                'reamount' => 0
            )
        );
        $descriptor->localtax1 = array(
            $cache_re[21]['sell'] => 0,
        );
        $descriptor->localtax2 = array(
            47510000 => 1.05,
        );
        $descriptor->source = 8;

        return $descriptor;
    }

    /**
     * {@inheritdoc}
     */
    public function getContrapartidas()
    {
        return array(
            43000004 => -5
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getApuntes()
    {
        $apuntes = array();

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->account = '70800000';
        $apunte->amount = 5;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->account = '47300000';
        $apunte->amount = 1.05;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->account = '43000004';
        $apunte->amount = 5;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->account = '47700021';
        $apunte->amount = 1.05;

        $apuntes[] = $apunte;

        return $apuntes;
    }

    /**
     * {@inheritdoc}
     */
    public function getExpectedFormat()
    {
        $expected_lf = '';

        // 21 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->apunte = 28;
        $invoiceformat->cuenta_iva = '70800000';
        $invoiceformat->bi = -5;
        $invoiceformat->iva_clave = 'GN';
        $invoiceformat->iva_tx = 21;
        $invoiceformat->iva_amount = -1.05;
        $invoiceformat->cuenta_factura = '43000004';
        $invoiceformat->fecha_factura = new \DateTime('07-01-2015');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        return $expected_lf;
    }
}