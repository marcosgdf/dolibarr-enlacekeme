<?php

namespace Marcosgdf\EnlaceKeme\Tests\Invoices\Customer;

require_once __DIR__.'/../../../enlacekeme/class/Apunte.php';
require_once __DIR__.'/../../TestInterfaces/InvoiceTestInterface.php';

use Marcosgdf\EnlaceKeme\InvoiceDescriptor;
use Marcosgdf\EnlaceKeme\Tests\TestInterfaces\InvoiceTestInterface;
use Marcosgdf\EnlaceKeme\Format\InvoiceFormat;
use Marcosgdf\EnlaceKeme\Apunte;

class Invoice2Test implements InvoiceTestInterface
{
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 6;
    }

    /**
     * {@inheritdoc}
     */
    public function getDate()
    {
        return new \DateTime('05-01-2015');
    }

    /**
     * {@inheritdoc}
     */
    public function getConcepto()
    {
        return 'Factura '.$this->getDocumento();
    }

    /**
     * {@inheritdoc}
     */
    public function getDocumento()
    {
        return 'FA1501-0002';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescriptor(array $cache_re)
    {
        $descriptor = new InvoiceDescriptor();

        $descriptor->date = "2015-01-05";
        $descriptor->ref = "FA1501-0002";
        $descriptor->type = InvoiceDescriptor::TYPE_GENERAL;
        $descriptor->total_ht = 10;
        $descriptor->total_ttc = 11.88;
        $descriptor->bases = array(
            70000000 => 10
        );
        $descriptor->iva = array(
            47700021 => array(
                'cuenta_base' => '70000000',
                'tx' => 21,
                'retx' => 5.2,
                'bi' => 5,
                'amount' => 1.05,
                'reamount' => 0.26
            ),
            47700010 => array(
                'cuenta_base' => '70000000',
                'tx' => 10,
                'retx' => 1.4,
                'bi' => 5,
                'amount' => 0.5,
                'reamount' => 0.07
            )
        );
        $descriptor->localtax1 = array(
            $cache_re[21]['sell'] => 0.26,
            $cache_re[10]['sell'] => 0.07
        );
        $descriptor->localtax2 = array(
            47510000 => 0
        );

        return $descriptor;
    }

    /**
     * {@inheritdoc}
     */
    public function getContrapartidas()
    {
        return array(
            43000002 => 11.88
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getApuntes()
    {
        $apuntes = array();

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->account = '70000000';
        $apunte->amount = 10;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->account = '47702152';
        $apunte->amount = 0.26;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->account = '47701014';
        $apunte->amount = 0.07;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->account = '43000002';
        $apunte->amount = 11.88;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->account = '47700021';
        $apunte->amount = 1.05;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->account = '47700010';
        $apunte->amount = 0.5;

        $apuntes[] = $apunte;

        return $apuntes;
    }

    /**
     * {@inheritdoc}
     */
    public function getExpectedFormat()
    {
        $expected_lf = '';

        // 21 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->apunte = 10;
        $invoiceformat->cuenta_iva = '70000000';
        $invoiceformat->bi = 5;
        $invoiceformat->iva_clave = 'GNR';
        $invoiceformat->iva_tx = 21;
        $invoiceformat->re_tx = 5.2;
        $invoiceformat->iva_amount = 1.05;
        $invoiceformat->re_amount = 0.26;
        $invoiceformat->cuenta_factura = '43000002';
        $invoiceformat->fecha_factura = new \DateTime('05-01-2015');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        // 10 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->apunte = 11;
        $invoiceformat->cuenta_iva = '70000000';
        $invoiceformat->bi = 5;
        $invoiceformat->iva_clave = 'RD2';
        $invoiceformat->iva_tx = 10;
        $invoiceformat->re_tx = 1.4;
        $invoiceformat->iva_amount = 0.5;
        $invoiceformat->re_amount = 0.07;
        $invoiceformat->cuenta_factura = '43000002';
        $invoiceformat->fecha_factura = new \DateTime('05-01-2015');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        return $expected_lf;
    }
}