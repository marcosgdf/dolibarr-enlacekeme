<?php

namespace Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier;

require_once __DIR__.'/../../../enlacekeme/class/Apunte.php';
require_once __DIR__.'/../../TestInterfaces/InvoiceTestInterface.php';

use Marcosgdf\EnlaceKeme\InvoiceDescriptor;
use Marcosgdf\EnlaceKeme\Format\InvoiceFormat;
use Marcosgdf\EnlaceKeme\Tests\TestInterfaces\InvoiceTestInterface;
use Marcosgdf\EnlaceKeme\Apunte;

class Invoice1Test implements InvoiceTestInterface
{
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 1;
    }

    /**
     * {@inheritdoc}
     */
    public function getDate()
    {
        return new \DateTime('13-10-2014');
    }

    /**
     * {@inheritdoc}
     */
    public function getConcepto()
    {
        return 'Factura PROV de proveedor';
    }

    /**
     * {@inheritdoc}
     */
    public function getDocumento()
    {
        return 'PROV';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescriptor(array $cache_re)
    {
        $descriptor = new InvoiceDescriptor();

        $descriptor->date = "2014-10-13";
        $descriptor->ref = "PROV";
        $descriptor->type = InvoiceDescriptor::TYPE_GENERAL;
        $descriptor->total_ht = 12;
        $descriptor->total_ttc = 13.05;
        $descriptor->bases = array(
            60000001 => 3,
            60000000 => 9
        );
        $descriptor->iva = array(
            47200021 => array(
                'cuenta_base' => '60000001',
                'tx' => 21,
                'retx' => 0,
                'bi' => 3,
                'amount' => 0.63,
                'reamount' => 0
            ),
            47200010 => array(
                'cuenta_base' => '60000000',
                'tx' => 10,
                'retx' => 0,
                'bi' => 3,
                'amount' => 0.3,
                'reamount' => 0
            ),
            47200004 => array(
                'cuenta_base' => '60000000',
                'tx' => 4,
                'retx' => 0,
                'bi' => 3,
                'amount' => 0.12,
                'reamount' => 0
            ),
            null => array(
                'cuenta_base' => '60000000',
                'tx' => 0,
                'retx' => 0,
                'bi' => 3,
                'amount' => 0,
                'reamount' => 0
            )
        );
        $descriptor->localtax1 = array(
            47202152 => 0,
            47201014 => 0,
            47200405 => 0,
            null => 0
        );
        $descriptor->localtax2 = array(
            47510000 => 0,
        );

        return $descriptor;
    }

    /**
     * {@inheritdoc}
     */
    public function getContrapartidas()
    {
        return array(
            '40000001' => 13.05
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getApuntes()
    {
        $apuntes = array();

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->amount = 3;
        $apunte->account = '60000001';

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->amount = 9;
        $apunte->account = '60000000';

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->amount = 13.05;
        $apunte->account = '40000001';

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->amount = 0.63;
        $apunte->account = '47200021';

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->amount = 0.3;
        $apunte->account = '47200010';

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->amount = 0.12;
        $apunte->account = '47200004';

        $apuntes[] = $apunte;

        return $apuntes;
    }

    /**
     * {@inheritdoc}
     */
    public function getExpectedFormat()
    {
        $expected_lf = '';

        // 21 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->soportado = 1;
        $invoiceformat->apunte = 4;
        $invoiceformat->cuenta_iva = '60000001';
        $invoiceformat->bi = 3;
        $invoiceformat->iva_clave = 'GN';
        $invoiceformat->iva_tx = 21;
        $invoiceformat->iva_amount = 0.63;
        $invoiceformat->cuenta_factura = '40000001';
        $invoiceformat->fecha_factura = new \DateTime('13-10-2014');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        // 10 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->soportado = 1;
        $invoiceformat->apunte = 5;
        $invoiceformat->cuenta_iva = '60000000';
        $invoiceformat->bi = 3;
        $invoiceformat->iva_clave = 'RD';
        $invoiceformat->iva_tx = 10;
        $invoiceformat->iva_amount = 0.3;
        $invoiceformat->cuenta_factura = '40000001';
        $invoiceformat->fecha_factura = new \DateTime('13-10-2014');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        //4 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->soportado = 1;
        $invoiceformat->apunte = 6;
        $invoiceformat->cuenta_iva = '60000000';
        $invoiceformat->bi = 3;
        $invoiceformat->iva_clave = 'SRD';
        $invoiceformat->iva_tx = 4;
        $invoiceformat->iva_amount = 0.12;
        $invoiceformat->cuenta_factura = '40000001';
        $invoiceformat->fecha_factura = new \DateTime('13-10-2014');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        //0 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->soportado = 1;
        $invoiceformat->apunte = 3;
        $invoiceformat->cuenta_iva = '60000000';
        $invoiceformat->bi = 3;
        $invoiceformat->cuenta_factura = '40000001';
        $invoiceformat->fecha_factura = new \DateTime('13-10-2014');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        return $expected_lf;
    }
}