<?php

namespace Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier;

require_once __DIR__.'/../../../enlacekeme/class/Apunte.php';
require_once __DIR__.'/../../TestInterfaces/InvoiceTestInterface.php';

use Marcosgdf\EnlaceKeme\Apunte;
use Marcosgdf\EnlaceKeme\InvoiceDescriptor;
use Marcosgdf\EnlaceKeme\Format\InvoiceFormat;
use Marcosgdf\EnlaceKeme\Tests\TestInterfaces\InvoiceTestInterface;

/**
 * Class Invoice7Test
 *
 * Adquisición Intracomunitaria de Bienes
 */
class Invoice7Test implements InvoiceTestInterface
{

    /**
     * Devuelve el ID de la factura
     *
     * @return int
     */
    public function getId()
    {
        return 7;
    }

    /**
     * Devuelve la fecha de la factura
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return new \DateTime('12-10-2015');
    }

    /**
     * Devuelve el concepto de la factura
     *
     * @return string
     */
    public function getConcepto()
    {
        return 'Factura 123 de proveedor';
    }

    /**
     * Devuelve el array que describe la factura
     *
     * @param array $cache_re Array que contiene los tipos impositivos del recargo de equivalencia
     * @return array
     */
    public function getDescriptor(array $cache_re)
    {
        $descriptor = new InvoiceDescriptor();

        $descriptor->date = '2015-10-12';
        $descriptor->ref = '123';
        $descriptor->type = InvoiceDescriptor::TYPE_GENERAL;
        $descriptor->total_ht = 1;
        $descriptor->total_ttc = 1;
        $descriptor->bases = array(
            60000000 => 1
        );
        $descriptor->iva = array(
            '' => array(
                'cuenta_base' => '60000000',
                'tx' => 0,
                'retx' => 0,
                'bi' => 1,
                'amount' => 0,
                'reamount' => 0
            )
        );
        $descriptor->localtax1 = array(
            '' => 0
        );
        $descriptor->localtax2 = array(
            47510000 => 0
        );

        return $descriptor;
    }

    /**
     * {@inheritdoc}
     */
    public function getContrapartidas()
    {
        return array(
            '40000005' => 1
        );
    }

    /**
     * Devuelve el nombre del documento de la factura
     *
     * @return string
     */
    public function getDocumento()
    {
        return '123';
    }

    /**
     * Devuelve los apuntes generados para la factura
     *
     * @return Apunte[]
     */
    public function getApuntes()
    {
        $apuntes = array();

        $apunte = new Apunte();
        $apunte->account = '60000000';
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->amount = 1;

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->account = '40000005';
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->amount = 1;

        $apuntes[] = $apunte;

        return $apuntes;
    }

    /**
     * Devuelve el formato exportado para EnlaceKeme de esta factura
     *
     * @return string
     */
    public function getExpectedFormat()
    {
        $expected_lf = '';

        // 0 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->soportado = 1;
        $invoiceformat->apunte = 32;
        $invoiceformat->cuenta_iva = '60000000';
        $invoiceformat->bi = 1;
        $invoiceformat->cuenta_factura = '40000005';
        $invoiceformat->fecha_factura = new \DateTime('12-10-2015');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;
        $invoiceformat->aib = true;

        $expected_lf .= $invoiceformat."\n";

        return $expected_lf;
    }
}