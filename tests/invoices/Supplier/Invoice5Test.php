<?php

namespace Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier;

require_once __DIR__.'/../../../enlacekeme/class/Apunte.php';
require_once __DIR__.'/../../TestInterfaces/InvoiceTestInterface.php';

use Marcosgdf\EnlaceKeme\InvoiceDescriptor;
use Marcosgdf\EnlaceKeme\Format\InvoiceFormat;
use Marcosgdf\EnlaceKeme\Tests\TestInterfaces\InvoiceTestInterface;
use Marcosgdf\EnlaceKeme\Apunte;

/**
 * Class Invoice5Test
 *
 * Devolución de servicios
 */
class Invoice5Test implements InvoiceTestInterface
{
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 5;
    }

    /**
     * {@inheritdoc}
     */
    public function getDate()
    {
        return new \DateTime('23-01-2015');
    }

    /**
     * {@inheritdoc}
     */
    public function getConcepto()
    {
        return 'Factura Rect.1 de proveedor';
    }

    /**
     * {@inheritdoc}
     */
    public function getDocumento()
    {
        return 'Rect.1';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescriptor(array $cache_re)
    {
        $descriptor = new InvoiceDescriptor();

        $descriptor->date = "2015-01-23";
        $descriptor->ref = "Rect.1";
        $descriptor->type = InvoiceDescriptor::TYPE_GENERAL;
        $descriptor->total_ht = -200;
        $descriptor->total_ttc = -242;
        $descriptor->bases = array(
            62900000 => -200,
        );
        $descriptor->iva = array(
            47200021 => array(
                'cuenta_base' => '62900000',
                'tx' => 21,
                'retx' => 0,
                'bi' => -200,
                'amount' => -42,
                'reamount' => 0
            ),
        );
        $descriptor->localtax1 = array(
            47202152 => 0,
        );
        $descriptor->localtax2 = array(
            47510000 => 0,
        );

        return $descriptor;
    }

    /**
     * {@inheritdoc}
     */
    public function getContrapartidas()
    {
        return array(
            '40000000' => -242
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getApuntes()
    {
        $apuntes = array();

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->amount = 200;
        $apunte->account = '62900000';

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->amount = 242;
        $apunte->account = '40000000';

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->amount = 42;
        $apunte->account = '47200021';

        $apuntes[] = $apunte;

        return $apuntes;
    }

    /**
     * {@inheritdoc}
     */
    public function getExpectedFormat()
    {
        $expected_lf = '';

        // 21 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->soportado = 1;
        $invoiceformat->apunte = 26;
        $invoiceformat->cuenta_iva = '62900000';
        $invoiceformat->bi = -200;
        $invoiceformat->iva_clave = 'GN';
        $invoiceformat->iva_tx = 21;
        $invoiceformat->iva_amount = -42;
        $invoiceformat->cuenta_factura = '40000000';
        $invoiceformat->fecha_factura = new \DateTime('23-01-2015');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        return $expected_lf;
    }
}