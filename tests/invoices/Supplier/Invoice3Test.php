<?php

namespace Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier;

require_once __DIR__.'/../../../enlacekeme/class/Apunte.php';
require_once __DIR__.'/../../TestInterfaces/InvoiceTestInterface.php';

use Marcosgdf\EnlaceKeme\InvoiceDescriptor;
use Marcosgdf\EnlaceKeme\Format\InvoiceFormat;
use Marcosgdf\EnlaceKeme\Tests\TestInterfaces\InvoiceTestInterface;
use Marcosgdf\EnlaceKeme\Apunte;

class Invoice3Test implements InvoiceTestInterface
{
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 3;
    }

    /**
     * {@inheritdoc}
     */
    public function getDate()
    {
        return new \DateTime('11-01-2015');
    }

    /**
     * {@inheritdoc}
     */
    public function getConcepto()
    {
        return 'Factura IRPF de proveedor';
    }

    /**
     * {@inheritdoc}
     */
    public function getDocumento()
    {
        return 'IRPF';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescriptor(array $cache_re)
    {
        $descriptor = new InvoiceDescriptor();

        $descriptor->date = "2015-01-11";
        $descriptor->ref = "IRPF";
        $descriptor->type = InvoiceDescriptor::TYPE_GENERAL;
        $descriptor->total_ht = 3;
        $descriptor->total_ttc = 3;
        $descriptor->bases = array(
            60000001 => 3,
        );
        $descriptor->iva = array(
            47200021 => array(
                'cuenta_base' => '60000001',
                'tx' => 21,
                'retx' => 0,
                'bi' => 3,
                'amount' => 0.63,
                'reamount' => 0
            ),
        );
        $descriptor->localtax1 = array(
            47202152 => 0,
        );
        $descriptor->localtax2 = array(
            47510000 => -0.63,
        );

        return $descriptor;
    }

    /**
     * {@inheritdoc}
     */
    public function getContrapartidas()
    {
        return array(
            '40000000' => 3
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getApuntes()
    {
        $apuntes = array();

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->amount = 3;
        $apunte->account = '60000001';

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->amount = 0.63;
        $apunte->account = '47510000';

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->amount = 3;
        $apunte->account = '40000000';

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->amount = 0.63;
        $apunte->account = '47200021';

        $apuntes[] = $apunte;

        return $apuntes;
    }

    /**
     * {@inheritdoc}
     */
    public function getExpectedFormat()
    {
        $expected_lf = '';

        // 21 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->soportado = 1;
        $invoiceformat->apunte = 19;
        $invoiceformat->cuenta_iva = '60000001';
        $invoiceformat->bi = 3;
        $invoiceformat->iva_clave = 'GN';
        $invoiceformat->iva_tx = 21;
        $invoiceformat->iva_amount = 0.63;
        $invoiceformat->cuenta_factura = '40000000';
        $invoiceformat->fecha_factura = new \DateTime('11-01-2015');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        return $expected_lf;
    }
}