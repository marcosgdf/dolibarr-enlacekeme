<?php

namespace Marcosgdf\EnlaceKeme\Tests\Invoices\Supplier;

require_once __DIR__.'/../../../enlacekeme/class/Apunte.php';
require_once __DIR__.'/../../TestInterfaces/InvoiceTestInterface.php';

use Marcosgdf\EnlaceKeme\InvoiceDescriptor;
use Marcosgdf\EnlaceKeme\Format\InvoiceFormat;
use Marcosgdf\EnlaceKeme\Tests\TestInterfaces\InvoiceTestInterface;
use Marcosgdf\EnlaceKeme\Apunte;

/**
 * Class Invoice4Test
 *
 * Devolución de productos
 */
class Invoice4Test implements InvoiceTestInterface
{
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 4;
    }

    /**
     * {@inheritdoc}
     */
    public function getDate()
    {
        return new \DateTime('21-01-2015');
    }

    /**
     * {@inheritdoc}
     */
    public function getConcepto()
    {
        return 'Factura DEVO de proveedor';
    }

    /**
     * {@inheritdoc}
     */
    public function getDocumento()
    {
        return 'DEVO';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescriptor(array $cache_re)
    {
        $descriptor = new InvoiceDescriptor();

        $descriptor->date = "2015-01-21";
        $descriptor->ref = "DEVO";
        $descriptor->type = InvoiceDescriptor::TYPE_GENERAL;
        $descriptor->total_ht = -100;
        $descriptor->total_ttc = -126.2;
        $descriptor->bases = array(
            60800000 => -100,
        );
        $descriptor->iva = array(
            47200021 => array(
                'cuenta_base' => '60800000',
                'tx' => 21,
                'retx' => 5.2,
                'bi' => -100,
                'amount' => -21,
                'reamount' => -5.2
            ),
        );
        $descriptor->localtax1 = array(
            47202152 => -5.2,
        );
        $descriptor->localtax2 = array(
            47510000 => 0,
        );

        return $descriptor;
    }

    /**
     * {@inheritdoc}
     */
    public function getContrapartidas()
    {
        return array(
            '40000002' => -126.2
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getApuntes()
    {
        $apuntes = array();

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->amount = 100;
        $apunte->account = '60800000';

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->amount = 5.2;
        $apunte->account = '47202152';

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_DEBE;
        $apunte->amount = 126.2;
        $apunte->account = '40000002';

        $apuntes[] = $apunte;

        $apunte = new Apunte();
        $apunte->type = Apunte::TYPE_HABER;
        $apunte->amount = 21;
        $apunte->account = '47200021';

        $apuntes[] = $apunte;

        return $apuntes;
    }

    /**
     * {@inheritdoc}
     */
    public function getExpectedFormat()
    {
        $expected_lf = '';

        // 21 % IVA
        $invoiceformat = new InvoiceFormat();
        $invoiceformat->soportado = 1;
        $invoiceformat->apunte = 23;
        $invoiceformat->cuenta_iva = '60800000';
        $invoiceformat->bi = -100;
        $invoiceformat->iva_clave = 'GNR';
        $invoiceformat->iva_tx = 21;
        $invoiceformat->re_tx = 5.2;
        $invoiceformat->iva_amount = -21;
        $invoiceformat->re_amount = -5.2;
        $invoiceformat->cuenta_factura = '40000002';
        $invoiceformat->fecha_factura = new \DateTime('21-01-2015');
        $invoiceformat->fechaop = $invoiceformat->fecha_factura;

        $expected_lf .= $invoiceformat."\n";

        return $expected_lf;
    }
}