<?php

namespace Marcosgdf\EnlaceKeme\Tests\Bag;

use Marcosgdf\EnlaceKeme\Bag\CommonBag;

class CommonBagTest extends \PHPUnit_Framework_TestCase
{
    public static function setUpBeforeClass()
    {
        require_once __DIR__.'/../../enlacekeme/class/Bag/CommonBag.php';
    }
    public function testToString()
    {
        $bag = new CommonBag('{test}');
        $bag->add('hi');
        $bag->add('ho');

        $expected = "{test}\nhi\nho\n";

        $this->assertEquals($expected, (string) $bag);
    }
}