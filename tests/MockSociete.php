<?php

class Societe
{
    public $country_code;

    public function isInEEC()
    {
        // List of all country codes that are in europe for european vat rules
        // List found on http://ec.europa.eu/taxation_customs/common/faq/faq_1179_en.htm#9
        $country_code_in_EEC=array(
            'AT',	// Austria
            'BE',	// Belgium
            'BG',	// Bulgaria
            'CY',	// Cyprus
            'CZ',	// Czech republic
            'DE',	// Germany
            'DK',	// Danemark
            'EE',	// Estonia
            'ES',	// Spain
            'FI',	// Finland
            'FR',	// France
            'GB',	// United Kingdom
            'GR',	// Greece
            'NL',	// Holland
            'HU',	// Hungary
            'IE',	// Ireland
            'IM',	// Isle of Man - Included in UK
            'IT',	// Italy
            'LT',	// Lithuania
            'LU',	// Luxembourg
            'LV',	// Latvia
            'MC',	// Monaco - Included in France
            'MT',	// Malta
            //'NO',	// Norway
            'PL',	// Poland
            'PT',	// Portugal
            'RO',	// Romania
            'SE',	// Sweden
            'SK',	// Slovakia
            'SI',	// Slovenia
            'UK',	// United Kingdom
            //'CH',	// Switzerland - No. Swizerland in not in EEC
        );
        //print "dd".$this->country_code;
        return in_array($this->country_code,$country_code_in_EEC);
    }
}