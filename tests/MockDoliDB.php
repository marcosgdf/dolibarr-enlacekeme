<?php

class MockDoliDB
{
    public static $result = false;
    public static $num_rows = 0;

    public function escape($string)
    {
        return $string;
    }

    public function query($string)
    {
        return true;
    }

    public function fetch_object()
    {
        return (object)self::$result;
    }

    public function num_rows()
    {
        return self::$num_rows;
    }
}