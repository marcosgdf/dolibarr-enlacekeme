<?php

namespace Marcosgdf\EnlaceKeme;

use Marcosgdf\EnlaceKeme\Bag\CommonBag;
use Marcosgdf\EnlaceKeme\Format\ApunteFormat;
use Marcosgdf\EnlaceKeme\Format\DatosauxiliaresFormat;
use Marcosgdf\EnlaceKeme\Format\InvoiceFormat;
use Marcosgdf\EnlaceKeme\Format\PlancontableFormat;

require 'DBException.php';
require_once 'Apunte.php';
require_once 'InvoiceDescriptor.php';
require_once 'Format/CommonFormat.php';
require_once 'Format/ApunteFormat.php';
require_once 'Format/InvoiceFormat.php';
require_once 'Format/PlancontableFormat.php';
require_once 'Format/DatosauxiliaresFormat.php';
require_once 'Bag/CommonBag.php';

/**
 * Clase que gestiona la exportación de datos al formato de Keme.
 */
class EnlaceKeme
{

    /**
     * Contador de los asientos
     *
     * @var int
     */
    public $asiento_counter = 1;

    /**
     * Contador de los apuntes
     *
     * @var int
     */
    public $apunte_counter = 1;

    /**
     * Resultado de la exportación del diario
     *
     * @var CommonBag
     */
    public $return_diario;

    /**
     * Resultado de la exportación del plan contable
     *
     * @var CommonBag
     */
    public $return_plancontable;

    /**
     * Resultado de la exportación de los datos auxiliares del plan contable
     *
     * @var CommonBag
     */
    public $return_datosauxiliares;

    /**
     * Resultado de la exportación de las facturas
     *
     * @var CommonBag
     */
    public $return_librofacturas;

    /**
     * Resultado de la exportación
     *
     * @var string
     */
    private $return = '';

    /**
     * Formato:
     * - tasa IVA => array(
     *      'sell' => cuenta
     *      'buy '=> cuenta
     * );
     *
     * @var array
     */
    public $cache_re = array();

    /**
     * Formato:
     * - tasa IVA => array(
     *      'ref' => valor,
     *      'ref_re' => valor
     * );
     *
     * @var array
     */
    public $cache_kemeref = array();

    public $cache_facturas = array();

    const EXPORT_CUSTOMERS = 1;
    const EXPORT_SUPPLIERS = 2;

    public function __construct()
    {
        $this->return_diario = new CommonBag('{diario}');
        $this->return_librofacturas = new CommonBag('{libro_facturas}');
        $this->return_plancontable = new CommonBag('{plancontable}');
        $this->return_datosauxiliares = new CommonBag('{datos_auxiliares}');
    }

    /**
     * Procesa los datos obtenidos de exportThirds
     *
     * Campos esperados para $result:
     *
     * - name
     * - zip
     * - town
     * - code_compta
     * - code_compta_fournisseur
     * - siren
     * - url
     * - address
     * - phone
     * - email
     * - fax
     * - country_code
     * - state
     *
     * @param int $type Tipo de la exportación: EXPORT_CUSTOMERS o EXPORT_SUPPLIERS
     * @param object $result Resultado de la query
     */
    private function processThirds($type, $result)
    {
        global $langs;

        if ($type == self::EXPORT_CUSTOMERS) {
            $code_compta = $result->code_compta;
        } else {
            $code_compta = $result->code_compta_fournisseur;
        }

        //Creación de la cuenta de tercero
        $plancontable = new PlancontableFormat();
        $plancontable->cuenta = $code_compta;
        $plancontable->nombre = $result->name;

        $this->return_plancontable->add($plancontable);

        if ($langs->trans('Country' . $result->country_code) != 'Country' . $result->country_code) {
            $country = $langs->transnoentities('Country' . $result->country_code);
        } else {
            $country = $result->country_code;
        }

        //Anotamos los datos auxiliares
        $datosauxiliares = new DatosauxiliaresFormat();
        $datosauxiliares->cuenta = $code_compta;
        $datosauxiliares->razon_social = $result->name;
        $datosauxiliares->nif = $result->siren;
        $datosauxiliares->web = $result->url;
        $datosauxiliares->domicilio = str_replace("\r\n", ", ", $result->address);
        $datosauxiliares->poblacion = $result->town;
        $datosauxiliares->cp = $result->zip;
        $datosauxiliares->provincia = $result->state;
        $datosauxiliares->telefono = $result->phone;
        $datosauxiliares->pais = $country;
        $datosauxiliares->fax = $result->fax;
        $datosauxiliares->email = $result->email;

        $this->return_datosauxiliares->add($datosauxiliares);
    }

    /**
     * Realiza una exportación de las cuentas contables accesorias de terceros
     *
     * @param int $type Tipo de exportación. Mirar las constantes EXPORT_
     * @throws DBException
     */
    public function exportThirds($type)
    {

        global $db;

        $sql = "SELECT s.nom as name, s.zip, s.town, s.code_compta, s.code_compta_fournisseur, s.siren, s.url";
        $sql .= ", s.address, s.phone, s.email, s.fax, p.code as country_code, d.nom as state";
        $sql .= " FROM " . MAIN_DB_PREFIX . "societe as s";
        $sql .= " LEFT JOIN " . MAIN_DB_PREFIX . "c_pays as p ON s.fk_pays = p.rowid";
        $sql .= " LEFT JOIN " . MAIN_DB_PREFIX . "c_departements as d ON s.fk_departement = d.rowid";

        if ($type == self::EXPORT_CUSTOMERS) {
            $sql .= " WHERE s.client IN (1,3)";
        } elseif ($type == self::EXPORT_SUPPLIERS) {
            $sql .= " WHERE s.fournisseur = 1";
        }

        if (!$query = $db->query($sql)) {
            throw new DBException($db->error());
        }

        while ($result = $db->fetch_object($query)) {
            $this->processThirds($type, $result);
        }
    }

    /**
     * Procesa el resultado de exportCustomerPayments.
     * Utilizado para testear individualmente.
     *
     * Campos esperados para $result:
     * - datep,
     * - amount,
     * - account_number,
     * - facnumber,
     * - code_compta
     *
     * @param object $result Resultado de la fila
     * @param bool $invert Invierte el texto cobro/pago. Usado para facturas de proveedores
     */
    private function processPayments($result, $invert = false)
    {
        $fecha = \DateTime::createFromFormat('Y-m-d H:i:s', $result->datep);
        $fecha->setTime(0, 0, 0);

        if ($result->amount < 0) {
            if ($invert) {
                $concepto = 'Cobro';
            } else {
                $concepto = 'Pago';
            }

            $apunte1 = Apunte::TYPE_HABER;
            $apunte2 = Apunte::TYPE_DEBE;
        } else {
            if ($invert) {
                $concepto = 'Pago';
            } else {
                $concepto = 'Cobro';
            }

            $apunte1 = Apunte::TYPE_DEBE;
            $apunte2 = Apunte::TYPE_HABER;
        }

        $concepto = $concepto . ' de la factura ' . $result->facnumber;

        $devengo = new Apunte();
        $devengo->account = $result->account_number;
        $devengo->amount = $result->amount;
        $devengo->type = $apunte1;

        $contrapartida = new Apunte();
        $contrapartida->account = $result->code_compta;
        $contrapartida->amount = $result->amount;
        $contrapartida->type = $apunte2;

        $this->createAsiento($fecha, $concepto, $result->facnumber, '', array(
            $devengo,
            $contrapartida
        ));
    }

    /**
     * Realiza una exportación de los pagos de clientes
     *
     * @param \DateTime $from Fecha de inicio
     * @param \DateTime $to Fecha de fin
     * @throws DBException
     */
    public function exportCustomerPayments(\DateTime $from, \DateTime $to)
    {
        global $db;

        $sql = "SELECT
  p.datep,
  p.amount,
  ba.account_number,
  f.facnumber,
  s.code_compta
FROM " . MAIN_DB_PREFIX . "paiement_facture pf LEFT JOIN " . MAIN_DB_PREFIX . "paiement p ON pf.fk_paiement = p.rowid
  LEFT JOIN " . MAIN_DB_PREFIX . "bank b ON p.fk_bank = b.rowid
  LEFT JOIN " . MAIN_DB_PREFIX . "bank_account ba ON b.fk_account = ba.rowid
  LEFT JOIN " . MAIN_DB_PREFIX . "facture f ON f.rowid = pf.fk_facture
  LEFT JOIN " . MAIN_DB_PREFIX . "societe s ON s.rowid = f.fk_soc
WHERE p.datep BETWEEN '" . $from->format('Y-m-d') . "' AND '" . $to->format('Y-m-d') . "'";

        if (!$query = $db->query($sql)) {
            throw new DBException($db->error());
        }

        while ($result = $db->fetch_object($query)) {
            $this->processPayments($result);
        }
    }

    /**
     * Realiza una exportación de los pagos de proveedores
     *
     * @param \DateTime $from Fecha de inicio
     * @param \DateTime $to Fecha de fin
     * @throws DBException
     */
    public function exportSupplierPayments(\DateTime $from, \DateTime $to)
    {

        global $db;

        $sql = "SELECT p.datep, p.amount, ba.account_number, f.ref_supplier as facnumber,";
        $sql .= "s.code_compta_fournisseur as code_compta FROM " . MAIN_DB_PREFIX . "paiementfourn_facturefourn pf
LEFT JOIN " . MAIN_DB_PREFIX . "paiementfourn p ON pf.fk_paiementfourn = p.rowid
LEFT JOIN " . MAIN_DB_PREFIX . "bank b ON p.fk_bank = b.rowid
LEFT JOIN " . MAIN_DB_PREFIX . "bank_account ba ON b.fk_account = ba.rowid
LEFT JOIN " . MAIN_DB_PREFIX . "facture_fourn f ON f.rowid = pf.fk_facturefourn
LEFT JOIN " . MAIN_DB_PREFIX . "societe s ON s.rowid = f.fk_soc
WHERE p.datep BETWEEN '" . $from->format('Y-m-d') . "' AND '" . $to->format('Y-m-d') . "'";

        if (!$query = $db->query($sql)) {
            throw new DBException($db->error());
        }

        while ($result = $db->fetch_object($query)) {
            $this->processPayments($result, true);
        }
    }

    /**
     * Realiza una exportación de las facturas de proveedores
     *
     * @param \DateTime $from Fecha de inicio
     * @param \DateTime $to Fecha de fin
     * @throws DBException
     */
    public function exportSupplierInvoices(\DateTime $from, \DateTime $to)
    {
        global $conf, $db;

        $p = explode(":", $conf->global->MAIN_INFO_SOCIETE_COUNTRY);
        $idpays = $p[0];

        $sql = "SELECT f.rowid, f.ref_supplier as facnumber, f.type, f.datef, f.libelle,";
        $sql .= " fd.total_ttc, fd.tva_tx, fd.total_ht, fd.tva as total_tva, 0 as deposit, fd.product_type, ";
        $sql .= "fd.localtax1_tx, fd.localtax2_tx, fd.total_localtax1, fd.total_localtax2,";
        $sql .= " s.nom as name, s.code_compta_fournisseur as code_compta,";
        $sql .= " p.ref as ref, p.accountancy_code_buy as accountancy_code,";
        $sql .= " ct.accountancy_code_buy as account_tva, ct.recuperableonly, null as fk_facture_source";
        $sql .= " FROM " . MAIN_DB_PREFIX . "facture_fourn_det fd";
        $sql .= " LEFT JOIN " . MAIN_DB_PREFIX . "c_tva ct ON fd.tva_tx = ct.taux";
        $sql .= " AND fd.info_bits = ct.recuperableonly AND ct.fk_pays = '" . $idpays . "'";
        $sql .= " LEFT JOIN " . MAIN_DB_PREFIX . "product p ON p.rowid = fd.fk_product";
        $sql .= " JOIN " . MAIN_DB_PREFIX . "facture_fourn f ON f.rowid = fd.fk_facture_fourn";
        $sql .= " JOIN " . MAIN_DB_PREFIX . "societe s ON s.rowid = f.fk_soc";
        $sql .= " WHERE f.fk_statut > 0 AND f.entity = " . $conf->entity;
        if (!empty($conf->global->FACTURE_DEPOSITS_ARE_JUST_PAYMENTS)) {
            $sql .= " AND f.type IN (0,1,2)";
        } else {
            $sql .= " AND f.type IN (0,1,2,3)";
        }
        $sql .= " AND f.datef BETWEEN '" . $from->format('Y-m-d') . "' AND '" . $to->format('Y-m-d') . "'";
        $sql .= " ORDER BY f.rowid, tva_tx DESC";

        if (in_array($db->type, array('mysql', 'mysqli'))) {
            $db->query('SET SQL_BIG_SELECTS=1');
        }

        if (!$result = $db->query($sql)) {
            throw new DBException($db->error());
        }

        $tabfac = array();
        $tabttc = array();

        while ($obj = $db->fetch_object($result)) {

            $this->processInvoiceQueryResult(
                $obj,
                false,
                $tabfac,
                $tabttc
            );

        }

        $this->processInvoices($tabfac, $tabttc, true);
    }

    /**
     * Procesa las facturas.
     *
     * @param InvoiceDescriptor[] $tabfac Array asociativo que contiene las facturas
     * @param array $tabttc Array asociativo que contiene la contrapartida
     * @param bool  $supplier ¿Estamos procesando fras. de clientes o proveedores?
     * @throws \Exception En caso de error en la DB
     */
    public function processInvoices(array $tabfac, array $tabttc, $supplier)
    {
        global $conf;

        if ($supplier) {
            $invert = true;
        } else {
            $invert = false;
        }

        foreach ($tabfac as $key => &$val) {

            //Restamos 1 al sim_apuntecounter porque el apunte_counter muestra el valor del próximo apunte
            //y no del último
            $sim_apuntecounter = $this->apunte_counter - 1;

            $ref = $val->ref;

            $fecha = new \DateTime($val->date);

            if ($val->type == InvoiceDescriptor::TYPE_ANTICIPO) {
                $concepto_txt = 'Factura de anticipo ' . $ref;
            } elseif ($val->type == InvoiceDescriptor::TYPE_RECTIFICATIVA || ($val->type == InvoiceDescriptor::TYPE_ABONO)) {
                $concepto_txt = 'Factura rectificativa ' . $ref;

                //Es una factura rectificativa.
                //El tipo 1 supone una diferencia en importe pero siempre > 0.
                //La forma en la que lo vamos a tratar es compensando la anterior con la nueva.

                if ($val->type == InvoiceDescriptor::TYPE_RECTIFICATIVA) {
                    //Recuperamos la actual factura
                    $factura_orig = $this->getInvoiceDescriptorByInvoiceId($val->source);

                    //Restamos 1 - 2
                    //En caso de que sea un importe mayor a facturar, estará en negativo
                    //En caso contrario, positivo.
                    $fra_diff = self::restarFactura($factura_orig, $val);

                    //Por tanto modificamos los valores de esta factura para que no muestren el importe total
                    // si no la diferencia
                    $fra_diff->invert();

                    //Y alteramos el tabttc
                    foreach ($tabttc[$key] as $cuenta => &$tabttc_val) {
                        //Si es la cuenta de cliente, entonces lo que vamos a hacer es imputarle la diferencia
                        if (self::isCuentaCliente($cuenta)) {
                            $tabttc_val = $fra_diff->total_ttc;
                        }
                    }

                    //Sustituimos la factura actual por la diferencia
                    $val = $fra_diff;
                }
            } else {
                if ($supplier) {
                    $concepto_txt = 'Factura '.$ref.' de proveedor';
                } else {
                    $concepto_txt = 'Factura '.$ref;
                }
            }

            $cuenta_tercero = null;

            $apuntes = array();

            foreach ($val->bases as $cuenta => $valor) {
                $apunte = new Apunte();
                $apunte->account = $cuenta;
                $apunte->amount = $valor;
                $apunte->calcType($invert);

                $apuntes[] = $apunte;

                $sim_apuntecounter++;
            }

            //Recargo de equivalencia
            foreach ($val->localtax1 as $cuenta => $valor) {

                if ($valor == 0) {
                    continue;
                }

                $apunte = new Apunte();
                $apunte->account = $cuenta;
                $apunte->amount = $valor;
                $apunte->calcType($invert);

                $apuntes[] = $apunte;

                $sim_apuntecounter++;
            }

            //Si hay IRPF lo metemos "a pelo"
            if ($val->localtax2[key($val->localtax2)] != 0) {

                $valor = $val->localtax2[key($val->localtax2)];

                if ($supplier) {
                    $cuenta = $conf->global->ENLACEKEME_CUENTA_HAEAT_IRPF;
                } else {
                    $cuenta = $conf->global->ENLACEKEME_CUENTA_RET_IRPF;
                }

                $apunte = new Apunte();
                $apunte->account = $cuenta;
                $apunte->amount = $valor;
                $apunte->calcType($invert);

                $apuntes[] = $apunte;

                $sim_apuntecounter++;
            }

            foreach ($tabttc[$key] as $cuenta => $valor) {
                $apunte = new Apunte();
                $apunte->account = $cuenta;
                $apunte->amount = $valor;
                $apunte->calcType(!$invert);

                $apuntes[] = $apunte;

                $sim_apuntecounter++;
            }

            $cuenta_tercero = $cuenta;

            //Necesitamos saber el apunte en el que se encuentra el tercero para aquellas operaciones sin
            // IVA repercutido.
            $apunte_tercero = $sim_apuntecounter;

            foreach ($val->iva as $iva_cuenta => $iva_info) {

                //Si no hay IVA no se registra apunte!
                if ($iva_info['tx'] != 0) {
                    $apunte = new Apunte();
                    $apunte->account = $iva_cuenta;
                    $apunte->amount = $iva_info['amount'];
                    $apunte->calcType($invert);

                    $apuntes[] = $apunte;

                    ++$sim_apuntecounter;

                    $current_apunte = $sim_apuntecounter;
                } else {
                    //En el caso de no haber IVA iguala a la del tercero
                    $current_apunte = $apunte_tercero;
                }

                if (abs($iva_info['tx']) > 0) {
                    $ref_keme = $this->obtenerRelacionIVAFromTx(
                        abs($iva_info['tx']),
                        ($iva_info['retx'] > 0 ? true : false)
                    );
                } else {
                    $ref_keme = '';
                }

                $invoiceformat = new InvoiceFormat();

                if ($val->eu_transaction === InvoiceDescriptor::EUINVOICE_SERVICES) {
                    if ($supplier) {
                        $invoiceformat->ais = true;
                    } else {
                        $invoiceformat->pis = true;
                    }
                } elseif ($val->eu_transaction === InvoiceDescriptor::EUINVOICE_PRODUCTS) {
                    if ($supplier) {
                        $invoiceformat->aib = true;
                    } else {
                        $invoiceformat->eib = true;
                    }
                }

                $invoiceformat->apunte = $current_apunte;
                $invoiceformat->cuenta_iva = $iva_info['cuenta_base'];
                $invoiceformat->bi = $iva_info['bi'];
                $invoiceformat->iva_clave = $ref_keme;
                $invoiceformat->iva_tx = abs($iva_info['tx']);
                $invoiceformat->re_tx = abs($iva_info['retx']);
                $invoiceformat->iva_amount = $iva_info['amount'];
                $invoiceformat->re_amount = $iva_info['reamount'];
                $invoiceformat->cuenta_factura = $cuenta_tercero;
                $invoiceformat->fecha_factura = $fecha;
                $invoiceformat->soportado = ($supplier ? true : false);
                $invoiceformat->fechaop = $fecha;

                $this->return_librofacturas->add($invoiceformat);
            }

            $this->createAsiento($fecha, $concepto_txt, $ref, '', $apuntes);
        }
    }

    /**
     * Realiza una exportación de las facturas de clientes
     *
     * @param \DateTime $from Fecha de inicio
     * @param \DateTime $to Fecha de fin
     * @throws DBException
     */
    public function exportCustomerInvoices(\DateTime $from, \DateTime $to)
    {
        global $conf, $db;

        $p = explode(":", $conf->global->MAIN_INFO_SOCIETE_COUNTRY);
        $idpays = $p[0];

        $sql = "SELECT f.rowid, f.facnumber, f.type, f.datef, f.ref_client as ref, f.fk_facture_source,";
        $sql .= " fd.product_type, fd.total_ht, fd.total_tva, fd.tva_tx, fd.total_ttc, fd.localtax1_tx,";
        $sql .= " fd.localtax2_tx, fd.total_localtax1, fd.total_localtax2, ((fd.info_bits = 2)";
        $sql .= " and fd.description = '(DEPOSIT)') deposit, s.nom as third_name,";
        $sql .= " s.code_compta, s.code_client,";
        $sql .= " p.accountancy_code_sell as accountancy_code,";
        $sql .= " ct.accountancy_code_sell as account_tva, ct.recuperableonly";
        $sql .= " FROM " . MAIN_DB_PREFIX . "facturedet fd";
        $sql .= " LEFT JOIN " . MAIN_DB_PREFIX . "product p ON p.rowid = fd.fk_product";
        $sql .= " JOIN " . MAIN_DB_PREFIX . "facture f ON f.rowid = fd.fk_facture";
        $sql .= " JOIN " . MAIN_DB_PREFIX . "societe s ON s.rowid = f.fk_soc";
        $sql .= " LEFT JOIN " . MAIN_DB_PREFIX . "c_tva ct ON fd.tva_tx = ct.taux AND ct.fk_pays = '" . $idpays . "'";
        $sql .= " WHERE f.entity = " . $conf->entity;
        $sql .= " AND f.fk_statut > 0";
        if (!empty($conf->global->FACTURE_DEPOSITS_ARE_JUST_PAYMENTS)) {
            $sql .= " AND f.type IN (0,1,2)";
        } else {
            $sql .= " AND f.type IN (0,1,2,3)";
        }
        $sql .= " AND fd.product_type IN (0,1)";
        $sql .= " AND f.datef BETWEEN '" . $from->format('Y-m-d') . "' AND '" . $to->format('Y-m-d') . "'";
        $sql .= " ORDER BY f.rowid, deposit DESC";

        if (in_array($db->type, array('mysql', 'mysqli'))) {
            $db->query('SET SQL_BIG_SELECTS=1');
        }

        if (!$result = $db->query($sql)) {
            throw new DBException($db->error());
        }

        $tabfac = array();
        $tabttc = array();

        while ($obj = $db->fetch_object($result)) {
            $this->processInvoiceQueryResult(
                $obj,
                true,
                $tabfac,
                $tabttc
            );
        }

        $this->processInvoices($tabfac, $tabttc, false);
    }

    /**
     * Procesa la consulta de exportación de pagos de impuestos.
     * Se separa para facilitar el testo de la aplicación.
     *
     * Los campos que se esperan de result son:
     * - account_number
     * - label
     * - amount
     * - datep
     *
     * @param object $result Resultado de la query
     * @param string $cuenta_acreedora Cuenta acreedora del pago
     */
    private function processTaxPayments($result, $cuenta_acreedora)
    {
        $fecha = new \DateTime($result->datep);
        $concepto = $result->label;

        $devengo = new Apunte();
        $devengo->type = Apunte::TYPE_HABER;
        $devengo->amount = $result->amount;
        $devengo->account = $result->account_number;

        $contrapartida = new Apunte();
        $contrapartida->type = Apunte::TYPE_DEBE;
        $contrapartida->amount = $result->amount;
        $contrapartida->account = $cuenta_acreedora;

        $this->createAsiento($fecha, $concepto, '', '', array(
            $devengo,
            $contrapartida
        ));
    }

    /**
     * Realiza una exportación de los pagos de impuestos
     *
     * @param \DateTime $from Fecha de inicio
     * @param \DateTime $to Fecha de fin
     * @throws DBException
     */
    public function exportTaxPayments(\DateTime $from, \DateTime $to)
    {

        global $db, $conf;

        $sql = "SELECT a.account_number, t.label, t.amount, t.datep FROM " . MAIN_DB_PREFIX . "tva t
  LEFT JOIN " . MAIN_DB_PREFIX . "bank b ON b.rowid = t.fk_bank
  LEFT JOIN " . MAIN_DB_PREFIX . "bank_account a ON a.rowid = b.fk_account
  WHERE t.datep BETWEEN '" . $from->format('Y-m-d') . "' AND '" . $to->format('Y-m-d') . "'";

        if (!$query = $db->query($sql)) {
            throw new DBException($db->error());
        }

        while ($result = $db->fetch_object($query)) {
            $this->processTaxPayments($result, $conf->global->ENLACEKEME_CUENTA_HAEAT_IVA);
        }

        $sql = "SELECT a.account_number, t.label, t.amount, t.datep FROM " . MAIN_DB_PREFIX . "localtax t
  LEFT JOIN " . MAIN_DB_PREFIX . "bank b ON b.rowid = t.fk_bank
  LEFT JOIN " . MAIN_DB_PREFIX . "bank_account a ON a.rowid = b.fk_account
        WHERE t.datep BETWEEN '" . $from->format('Y-m-d') . "' AND '" . $to->format('Y-m-d') . "'";

        if (!$query = $db->query($sql)) {
            throw new DBException($db->error());
        }

        while ($result = $db->fetch_object($query)) {
            $this->processTaxPayments($result, $conf->global->ENLACEKEME_CUENTA_HAEAT_IRPF);
        }
    }

    /**
     * Devuelve las relaciones IVA con los códigos de IVA de Keme.
     *
     * @return array
     */
    public static function obtenerRelacionesIVA()
    {

        global $db;

        $return = array();

        $sql = 'SELECT t.rowid, t.note, t.taux iva, t.localtax1 re, k.ref as ref_iva,';
        $sql .= 'k.ref_re FROM ' . MAIN_DB_PREFIX . 'c_tva t LEFT OUTER JOIN ' . MAIN_DB_PREFIX . 'keme_relvat k ';
        $sql .= 'ON t.rowid = k.fk_vat WHERE t.fk_pays = 4';

        $result = $db->query($sql);

        while ($obj = $db->fetch_object($result)) {
            $return[$obj->rowid] = array(
                'note' => $obj->note,
                'rate' => $obj->iva,
                're' => $obj->re,
                'ref' => $obj->ref_iva,
                'ref_re' => $obj->ref_re
            );
        }

        return $return;

    }

    /**
     * Actualiza una relación de IVA KEME - Dolibarr
     *
     * @param int $id Id del impuesto a relacionar
     * @param string $ref_iva Código de KEME
     * @param string $ref_re Código de Keme para el IVA + RE
     * @return bool
     */
    public static function actualizarRelacionIVA($id, $ref_iva, $ref_re)
    {

        global $db;

        $sql = 'INSERT INTO
                  ' . MAIN_DB_PREFIX . 'keme_relvat (id, ref, ref_re, fk_vat)
                VALUES
                  (null, \'' . $db->escape($ref_iva) . '\', \'' . $db->escape($ref_re) . '\', ' . $id . ')
                ON DUPLICATE KEY UPDATE ref = VALUES(ref), ref_re = VALUES(ref_re);';

        if (!$db->query($sql)) {
            return false;
        }

        return true;
    }

    /**
     * Devuelve el código de IVA de Keme para la tasa buscada
     *
     * @param float $tx Tasa
     * @param bool $re ¿Recuperar código con RE?
     * @throws \Exception Error en la DB
     * @return string
     */
    private function obtenerRelacionIVAFromTx($tx, $re = null)
    {
        global $db;

        //Lo pasamos a string para evitar problemas con los index array
        $tx = (string)$tx;

        if (!$re || ($re == 0)) {
            $toselect = 'ref';
        } else {
            $toselect = 'ref_re';
        }

        if (isset($this->cache_kemeref[$tx][$toselect])) {
            return $this->cache_kemeref[$tx][$toselect];
        }

        $sql = "select $toselect from " . MAIN_DB_PREFIX . "keme_relvat v left join " . MAIN_DB_PREFIX . "c_tva t
        on v.fk_vat = t.rowid where t.fk_pays = 4 and t.taux = '" . (float)$tx . "'";

        if (!$query = $db->query($sql)) {
            throw new DBException($db->error());
        }

        $result = $db->fetch_object($query);

        $this->cache_kemeref[$tx][$toselect] = $result->$toselect;

        return $result->$toselect;
    }

    /**
     * Devuelve la cuenta del Recargo de equivalencia asociado a la tasa de IVA especificada
     *
     * @param float $tx Tasa
     * @param boolean $ventas ¿Debemos buscar el
     * @throws \Exception Error en la DB
     * @return string
     */
    private function obtenerCuentaFromIVA($tx, $ventas = false)
    {
        global $db;

        //Lo pasamos a string para evitar problemas con los index array
        $tx = (string)$tx;

        if (isset($this->cache_re[$tx])) {
            if ($ventas) {
                return $this->cache_re[$tx]['sell'];
            } else {
                return $this->cache_re[$tx]['buy'];
            }
        }

        if ($ventas) {
            $prefix = 'ENLACEKEME_CUENTA_RE_SELL';
            $array_prefix = 'sell';
        } else {
            $prefix = 'ENLACEKEME_CUENTA_RE_BUY';
            $array_prefix = 'buy';
        }

        $sql = "SELECT c.value FROM " . MAIN_DB_PREFIX . "c_tva t
LEFT JOIN " . MAIN_DB_PREFIX . "const c ON c.`name` = CONCAT('" . $prefix . "_', t.rowid)
WHERE t.fk_pays = 4 AND t.taux = '" . (float)$db->escape($tx) . "'";

        if (!$query = $db->query($sql)) {
            throw new DBException($db->error());
        }

        $result = $db->fetch_object($query);

        $this->cache_re[$tx][$array_prefix] = $result->value;

        return $result->value;

    }

    /**
     * Devuelve el resultado del contenido del archivo de exportación
     *
     * @return string
     */
    public function getResult()
    {

        $this->return .= $this->return_plancontable;
        $this->return .= "\n" . $this->return_datosauxiliares;
        $this->return .= "\n" . $this->return_diario;
        $this->return .= "\n" . $this->return_librofacturas;

        return $this->return;
    }

    /**
     * Función auxiliar de creación de asientos.
     * Formato de los apuntes:
     *
     * array(
     *      'debe' => float,
     *      'haber' => float,
     *      'cuenta' => string
     * )
     *
     * Se debe elegir entre debe o haber
     *
     * @param \DateTime $fecha Fecha del asiento
     * @param string $concepto Concepto del asiento
     * @param string $documento Documento del asiento
     * @param string $codfactura Código de la factura (utilizado en documentos de proveedores)
     * @param Apunte[] $apuntes Apuntes según formato
     * @return true
     */
    private function createAsiento(\DateTime $fecha, $concepto, $documento, $codfactura, array $apuntes)
    {
        foreach ($apuntes as $apunte) {

            $newapunte = new ApunteFormat();

            if ($apunte->type == Apunte::TYPE_DEBE) {
                $newapunte->debe = (float)$apunte->getAmount();
            }

            if ($apunte->type == Apunte::TYPE_HABER) {
                $newapunte->haber = (float)$apunte->getAmount();
            }

            $newapunte->numero = $this->asiento_counter;
            $newapunte->apunte = $this->apunte_counter;
            $newapunte->fecha = $fecha;
            $newapunte->cuenta = (string) $apunte->account;
            $newapunte->concepto = $concepto;
            $newapunte->documento = $documento;
            $newapunte->codfactura = $codfactura;

            $this->return_diario->add($newapunte);

            $this->apunte_counter++;
        }

        $this->asiento_counter++;
    }

    /**
     * Comprueba que todos los códigos contables sean los correctos
     * @return array Devuelve un array con los mensajes de error, vacío si correcto
     * @throws DBException
     */
    public function checkAccountancyCodes()
    {

        global $db;

        $errors = array();

        $checks = array(
            array(
                'sql' => "SELECT rowid FROM " . MAIN_DB_PREFIX . "const WHERE `name` IN(
							'COMPTA_PRODUCT_BUY_ACCOUNT',
							'COMPTA_PRODUCT_SOLD_ACCOUNT',
							'COMPTA_SERVICE_BUY_ACCOUNT',
							'COMPTA_SERVICE_SOLD_ACCOUNT',
						    'COMPTA_VAT_ACCOUNT',
						    'COMPTA_VAT_BUY_ACCOUNT',
						    'COMPTA_ACCOUNT_CUSTOMER',
						    'COMPTA_ACCOUNT_SUPPLIER'
						) AND `value` != ''",
                'num_rows' => 8,
                'errormsg' => 'La configuración del módulo de contabilidad no está completa.'
            ),
            array(
                'sql' => "SELECT rowid FROM " . MAIN_DB_PREFIX . "const WHERE `name` IN(
						    'ENLACEKEME_CUENTA_HAEAT_IRPF',
						    'ENLACEKEME_CUENTA_RET_IRPF',
						    'ENLACEKEME_CUENTA_DEVO_COMPRAS',
						    'ENLACEKEME_CUENTA_DEVO_VENTAS',
						    'ENLACEKEME_CUENTA_HAEAT_IVA',
						    'ENLACEKEME_PREFIJOCUENTA_ANTICIPOS'
						) AND `value` != ''",
                'num_rows' => 6,
                'errormsg' => 'La configuración del módulo Enlace Keme no está completa.'
            ),
            array(
                'sql' => "SELECT rowid FROM " . MAIN_DB_PREFIX . "bank_account b WHERE b.account_number = ''
                OR b.account_number IS NULL",
                'errormsg' => 'Existen bancos que no tienen asociada una cuenta contable.'
            ),
            array(
                'sql' => "SELECT * FROM " . MAIN_DB_PREFIX . "c_tva WHERE `fk_pays`= 4 AND `taux` > 0
                AND (accountancy_code_sell IS NULL OR accountancy_code_buy IS NULL)",
                'errormsg' => 'Existen impuestos que no tienen asociada una cuenta contable.'
            ),
            array(
                'sql' => "SELECT rowid FROM " . MAIN_DB_PREFIX . "const WHERE `name` = 'COMPTA_MODE'
                AND `value` != 'CREANCES-DETTES'",
                'errormsg' => 'La configuración del módulo de contabilidad no es la correcta.'
            )
        );

        foreach ($checks as $check) {

            if (!$query = $db->query($check['sql'])) {
                throw new DBException($db->error());
            }

            if (isset($check['num_rows'])) {
                if ($db->num_rows($query) != $check['num_rows']) {
                    $errors[] = $check['errormsg'];
                }
            } elseif ($db->num_rows($query) > 0) {
                $errors[] = $check['errormsg'];
            }

        }

        return $errors;
    }

    /**
     * Resta facturas para su correcta contabilización
     *
     * @param InvoiceDescriptor $factura1 Factura 1
     * @param InvoiceDescriptor $factura2 Factura 2
     * @return InvoiceDescriptor
     */
    public static function restarFactura(InvoiceDescriptor $factura1, InvoiceDescriptor $factura2)
    {
        //B.I.
        $total_ht = $factura1->total_ht - $factura2->total_ht;

        //Total
        $total_ttc = $factura1->total_ttc - $factura2->total_ttc;

        //Bases imponibles
        $bases = array();

        //IVA
        $iva = array();

        //Recargo de equivalencia
        $localtax1 = array();

        //IRPF
        $localtax2 = array();

        //FRA_EUTYPE
        $fra_eutype = $factura2->eu_transaction;

        /**
         * IVA
         */

        //Restamos los IVAS de la primera factura a la segunda
        foreach ($factura1->iva as $cuenta => $ivainfo) {
            $iva[$cuenta] = array(
                'cuenta_base' => $ivainfo['cuenta_base'],
                'tx' => $ivainfo['tx'],
                'retx' => $ivainfo['retx'],
                'bi' => $ivainfo['bi'] - $factura2->iva[$cuenta]['bi'],
                'amount' => $ivainfo['amount'] - $factura2->iva[$cuenta]['amount'],
                'reamount' => $ivainfo['reamount'] - $factura2->iva[$cuenta]['reamount'],
            );
        }

        //Comprobamos si hay algún IVA de la segunda factura que no esté presente en la primera
        $factura2ivadiff = array_diff(
            array_keys($factura2->iva),
            array_keys($factura1->iva)
        );

        foreach ($factura2ivadiff as $cuentanopresente) {

            $ivainfo = $factura2->iva[$cuentanopresente];

            $iva[$cuentanopresente] = array(
                'cuenta_base' => $ivainfo['cuenta_base'],
                'tx' => $ivainfo['tx'],
                'retx' => $ivainfo['retx'],
                'bi' => $ivainfo['bi'],
                'amount' => $ivainfo['amount'],
                'reamount' => $ivainfo['reamount']
            );
        }

        /**
         * RE
         */

        //Restamos los RE de la primera factura a la segunda
        foreach ($factura1->localtax1 as $cuenta => $amount) {
            $localtax1[$cuenta] = $amount - $factura2->localtax1[$cuenta];
        }

        //Comprobamos si hay algún RE de la segunda factura que no esté presente en la primera
        $factura2lt1diff = array_diff(
            array_keys($factura2->localtax1),
            array_keys($factura1->localtax1)
        );

        foreach ($factura2lt1diff as $cuentanopresente) {

            $amount = $factura2->localtax1[$cuentanopresente];

            $localtax1[$cuentanopresente] = $amount;
        }

        /**
         * IRPF
         */

        //Restamos los IRPF de la primera factura a la segunda
        foreach ($factura1->localtax2 as $cuenta => $amount) {
            $localtax2[$cuenta] = $amount - $factura2->localtax2[$cuenta];
        }

        //Comprobamos si hay algún IRPF de la segunda factura que no esté presente en la primera
        $factura2lt2diff = array_diff(
            array_keys($factura2->localtax2),
            array_keys($factura1->localtax2)
        );

        foreach ($factura2lt2diff as $cuentanopresente) {

            $amount = $factura2->localtax2[$cuentanopresente];

            $localtax2[$cuentanopresente] = $amount;
        }

        /**
         * BASES
         */

        foreach ($factura1->bases as $cuenta => $amount) {
            $bases[$cuenta] = $amount - $factura2->bases[$cuenta];
        }

        //Comprobamos si hay alguna base de la segunda factura que no esté presente en la primera
        $factura2basesdiff = array_diff(
            array_keys($factura2->bases),
            array_keys($factura1->bases)
        );

        foreach ($factura2basesdiff as $cuentanopresente) {

            $amount = $factura2->bases[$cuentanopresente];

            $bases[$cuentanopresente] = $amount;
        }

        $return = new InvoiceDescriptor();
        $return->total_ht = $total_ht;
        $return->total_ttc = $total_ttc;
        $return->bases = $bases;
        $return->iva = $iva;
        $return->localtax1 = $localtax1;
        $return->localtax2 = $localtax2;
        $return->eu_transaction = $fra_eutype;

        return $return;
    }

    /**
     * Obtiene el InvoiceDescriptor a partir del ID de una factura
     *
     * @param int $fraid Id de la factura
     * @return InvoiceDescriptor
     * @throws DBException
     */
    private function getInvoiceDescriptorByInvoiceId($fraid)
    {
        global $db, $conf;

        //Utilizado para facilitar los tests de unidad
        if (isset($this->cache_facturas[$fraid])) {
            return $this->cache_facturas[$fraid];
        }

        $p = explode(":", $conf->global->MAIN_INFO_SOCIETE_COUNTRY);
        $idpays = $p[0];

        $sql = "SELECT f.rowid, f.facnumber, f.type, f.datef, f.ref_client as ref, f.fk_facture_source,";
        $sql .= " fd.product_type, fd.total_ht, fd.total_tva, fd.tva_tx, fd.total_ttc, fd.localtax1_tx, ";
        $sql .= "fd.localtax2_tx, fd.total_localtax1, fd.total_localtax2, ((fd.info_bits = 2) ";
        $sql .= "and fd.description = '(DEPOSIT)') deposit, s.nom as third_name";
        $sql .= " s.code_compta, s.code_client,";
        $sql .= " p.accountancy_code_sell,";
        $sql .= " ct.accountancy_code_sell as account_tva, ct.recuperableonly";
        $sql .= " FROM " . MAIN_DB_PREFIX . "facturedet fd";
        $sql .= " LEFT JOIN " . MAIN_DB_PREFIX . "product p ON p.rowid = fd.fk_product";
        $sql .= " JOIN " . MAIN_DB_PREFIX . "facture f ON f.rowid = fd.fk_facture";
        $sql .= " JOIN " . MAIN_DB_PREFIX . "societe s ON s.rowid = f.fk_soc";
        $sql .= " LEFT JOIN " . MAIN_DB_PREFIX . "c_tva ct ON fd.tva_tx = ct.taux AND ct.fk_pays = '" . $idpays . "'";
        $sql .= " WHERE f.rowid = '" . $db->escape($fraid) . "'";

        if (in_array($db->type, array('mysql', 'mysqli'))) {
            $db->query('SET SQL_BIG_SELECTS=1');
        }

        if (!$result = $db->query($sql)) {
            throw new DBException($db->error());
        }

        $tabfac = array();
        $tabttc = array();

        while ($obj = $db->fetch_object($result)) {

            $this->processInvoiceQueryResult(
                $obj,
                true,
                $tabfac,
                $tabttc
            );
        }

        return $tabfac[$fraid];
    }

    /**
     * ¿Es una cuenta de cliente?
     * Llegaremos a esa conclusión si empieza por 430
     *
     * @param string $cuenta La cuenta contable
     * @return bool
     */
    public static function isCuentaCliente($cuenta)
    {
        if (preg_match('/^430[0-9]*$/', $cuenta)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Convierte a array el resultado de la query de consulta de facturas
     *
     * El formato esperado de $obj es:
     * - rowid: Id de factura
     * - facnumber: Referencia de factura
     * - type: Tipo de factura
     * - datef: Fecha de factura
     * - ref: Referencia de factura de cliente
     * - fk_facture_source: Origen de la factura (para rectificativas, etc)
     * - product_type: Producto/Servicio
     * - total_ht: Base imponible
     * - total_tva: IVA total
     * - tva_tx: Tasa de IVA
     * - total_ttc: Importe total de factura
     * - localtax1_tx: Tasa de RE
     * - localtax2_tx: Tasa de IRPF
     * - total_localtax1: Importe total de RE de la línea
     * - total_localtax2: Importe total de IRPF de la línea
     * - deposit: ¿Corresponde la línea a un descuento por anticipo?
     * - third_name: Nombre del tercero
     * - code_compta: Cuenta contable del cliente
     * - code_client: Código de cliente
     * - accountancy_code: Cuenta contable de venta
     * - account_tva: Cuenta contable de IVA
     * - recuperableonly
     *
     * @param object $obj Resultado de la query de consulta de facturas
     * @param bool $ventas ¿Es una operación de venta?
     * @param InvoiceDescriptor[] $tabfac Array con el contenido de $tabfac
     * @param array $tabttc Array con el contenido de $tabttc
     * @return array
     */
    private function processInvoiceQueryResult($obj, $ventas, &$tabfac, &$tabttc)
    {
        global $conf;

        if ($ventas) {
            $cptcli = $conf->global->COMPTA_ACCOUNT_CUSTOMER;
            $account_product = $conf->global->COMPTA_PRODUCT_SOLD_ACCOUNT;
            $account_service = $conf->global->COMPTA_SERVICE_SOLD_ACCOUNT;
        } else {
            $cptcli = $conf->global->COMPTA_ACCOUNT_SUPPLIER;
            $account_product = $conf->global->COMPTA_PRODUCT_BUY_ACCOUNT;
            $account_service = $conf->global->COMPTA_SERVICE_BUY_ACCOUNT;
        }

        // les variables
        $compta_soc = (!empty($obj->code_compta) ? $obj->code_compta : $cptcli);

        if (!$obj->accountancy_code) {
            if ($obj->product_type == 0) {
                $compta_prod = $account_product;
            } else {
                $compta_prod = $account_service;
            }
        } else {
            $compta_prod = $obj->accountancy_code;
        }

        //Si es una factura de anticipo la cuenta va a ser la definida en el módulo
        if ($obj->type == 3) {
            $compta_prod = $conf->global->ENLACEKEME_PREFIJOCUENTA_ANTICIPOS . $obj->code_client;
        }

        //Si esta línea corresponde a un anticipo de cliente, vamos a imputarlo contra esa cuenta
        if ($obj->deposit == 1) {
            $compta_soc = $conf->global->ENLACEKEME_PREFIJOCUENTA_ANTICIPOS . $obj->code_client;

            //Nos aseguramos de que la cuenta existe
            $plancontable = new PlancontableFormat();
            $plancontable->cuenta = $compta_soc;
            $plancontable->nombre = $obj->third_name;

            $this->return_plancontable->add($plancontable);
        }

        if ($obj->tva_tx > 0) {
            //Cuenta del recargo de equivalencia. Recurrimos a la configuración del módulo para recuperarla
            $compta_localtax1 = $this->obtenerCuentaFromIVA($obj->tva_tx, $ventas);

            if (empty($obj->account_tva)) {
                throw new \RuntimeException('Las cuentas de IVA no están completamente definidas.');
            }

            $compta_tva = $obj->account_tva;
        } else {
            $compta_localtax1 = null;
            $compta_tva = null;
        }

        //Cuenta del irpf. Recurrimos a la configuración del módulo para recuperarla
        $compta_localtax2 = $conf->global->ENLACEKEME_CUENTA_HAEAT_IRPF;

        //Inicialización de campos
        if (!isset($tabfac[$obj->rowid])) {
            $id = new InvoiceDescriptor();
            $id->date = $obj->datef;
            $id->ref = $obj->facnumber;
            $id->type = $obj->type;

            if ($obj->fk_facture_source) {
                $id->source = $obj->fk_facture_source;
            }

            $id->eu_transaction = $this->checkEUInvoiceType(!$ventas, $obj->rowid);

            $tabfac[$obj->rowid] = $id;
        }
        if (!isset($tabttc[$obj->rowid][$compta_soc])) {
            $tabttc[$obj->rowid][$compta_soc] = 0;
        }
        if (!isset($tabfac[$obj->rowid]->localtax1[$compta_localtax1])) {
            $tabfac[$obj->rowid]->localtax1[$compta_localtax1] = 0;
        }
        if (!isset($tabfac[$obj->rowid]->localtax2[$compta_localtax2])) {
            $tabfac[$obj->rowid]->localtax2[$compta_localtax2] = 0;
        }
        if (!isset($tabfac[$obj->rowid]->iva[$compta_tva])) {
            $tabfac[$obj->rowid]->iva[$compta_tva] = array(
                'cuenta_base' => '',
                'tx' => 0,
                'retx' => 0,
                'bi' => 0,
                'amount' => 0,
                'reamount' => 0
            );
        }

        //Si esta línea corresponde a un anticipo de cliente, no hay IVA de por medio
        //Y el resto, debiera imputarse contra la cuenta del cliente
        if ($obj->deposit == 1) {

            $compta_third = (!empty($obj->code_compta) ? $obj->code_compta : $cptcli);

            if (!isset($tabttc[$obj->rowid][$compta_third])) {
                $tabttc[$obj->rowid][$compta_third] = 0;
            }

            $tabttc[$obj->rowid][$compta_soc] += abs($obj->total_ht);
            $tabttc[$obj->rowid][$compta_third] += $obj->total_ttc;
        } else {
            $tabttc[$obj->rowid][$compta_soc] += $obj->total_ttc;
        }

        //En caso de que sea una devolución, la cuenta base es esta.

        if ($obj->total_ht < 0) {
            if (!$ventas) {
                if ($obj->product_type === 0) {
                    $compta_prod = $conf->global->ENLACEKEME_CUENTA_DEVO_COMPRAS;
                } else {
                    $compta_prod = $conf->global->COMPTA_SERVICE_BUY_ACCOUNT;
                }
            } else {
                $compta_prod = $conf->global->ENLACEKEME_CUENTA_DEVO_VENTAS;
            }
        }

        //Si la línea es de anticipo entonces no minora el importe de la venta
        if ($obj->deposit != 1) {

            if (!isset($tabfac[$obj->rowid]->bases[$compta_prod])) {
                $tabfac[$obj->rowid]->bases[$compta_prod] = 0;
            }

            $tabfac[$obj->rowid]->bases[$compta_prod] += $obj->total_ht;
        }

        if ($obj->recuperableonly != 1) {

            /**
             * Si es la misma cuenta, la ponemos.
             * Si reviste complejidad en su obtención (+ de 1 por IVA) es opcional
             *
             * Hacemos check del deposit porque deposit no es una cuenta de base de factura.
             */
            if ($obj->deposit != 1) {
                if (!$tabfac[$obj->rowid]->iva[$compta_tva]['cuenta_base']) {
                    $tabfac[$obj->rowid]->iva[$compta_tva]['cuenta_base'] = $compta_prod;
                } elseif ($tabfac[$obj->rowid]->iva[$compta_tva]['cuenta_base'] != $compta_prod) {
                    $tabfac[$obj->rowid]->iva[$compta_tva]['cuenta_base'] = '';
                }
            }

            $tabfac[$obj->rowid]->iva[$compta_tva]['bi'] += $obj->total_ht;
            $tabfac[$obj->rowid]->iva[$compta_tva]['tx'] = $obj->tva_tx;
            $tabfac[$obj->rowid]->iva[$compta_tva]['retx'] = $obj->localtax1_tx;
            $tabfac[$obj->rowid]->iva[$compta_tva]['amount'] += $obj->total_tva;
            $tabfac[$obj->rowid]->iva[$compta_tva]['reamount'] += $obj->total_localtax1;
        }

        $tabfac[$obj->rowid]->localtax1[$compta_localtax1] += $obj->total_localtax1;

        //Para trampear el bug https://doliforge.org/tracker/?func=detail&aid=1786&group_id=144
        if ($obj->type == 2) {
            $tabfac[$obj->rowid]->localtax2[$compta_localtax2] += -$obj->total_localtax2;
        } else {
            $tabfac[$obj->rowid]->localtax2[$compta_localtax2] += $obj->total_localtax2;
        }

        $tabfac[$obj->rowid]->total_ht += $obj->total_ht;
        $tabfac[$obj->rowid]->total_ttc += $obj->total_ttc;
    }

    /**
     * Devuelve el tipo de transacción intracomunitaria
     *
     * @param bool $supplier ¿Es proveedor?
     * @param int $facid Id de la factura
     * @return false|int False si no, self::EUINVOICE_PRODUCTS si producto o self::EUINVOICE_SERVICES si servicio
     * @throws DBException
     */
    public function checkEUInvoiceType($supplier, $facid)
    {
        global $db;

        if ($supplier) {
            $table = "llx_facture_fourn";
            $tabledet = "llx_facture_fourn_det";
            $fk = "fk_facture_fourn";
        } else {
            $table = "llx_facture";
            $tabledet = "llx_facturedet";
            $fk = "fk_facture";
        }

        $sql = "SELECT
  IF(((fd.info_bits = 2) AND fd.description = '(DEPOSIT)'), 1, fd.product_type) ptype,
  SUM(fd.total_ht)                                                              sum,
  s.tva_intra,
  cp.code
FROM $tabledet fd LEFT JOIN llx_product p ON p.rowid = fd.fk_product
  JOIN $table f ON f.rowid = fd.$fk
  LEFT JOIN llx_societe s ON s.rowid = f.fk_soc
  LEFT JOIN llx_c_pays cp ON cp.rowid = s.fk_pays
WHERE f.rowid = ".$db->escape($facid)."
GROUP BY ptype
ORDER BY sum DESC
LIMIT 1;";

        if (!$result = $db->query($sql)) {
            throw new DBException($db->error());
        }

        if (!$db->num_rows($result)) {
            return false;
        }

        $obj = $db->fetch_object($result);

        $soc = new \Societe($db);
        $soc->country_code = $obj->code;

        if (!empty($obj->tva_intra) && $soc->isInEEC()) {
            return (int) $obj->ptype;
        }

        return false;
    }
}
