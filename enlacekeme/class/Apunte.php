<?php

namespace Marcosgdf\EnlaceKeme;

/**
 * Clase que representa un apunte internamente.
 * @package Marcosgdf\EnlaceKeme
 */
class Apunte
{

    /**
     * @var string Tipo del apunte: Asiento::TYPE_DEBE o Asiento::TYPE_HABER
     */
    public $type;

    /**
     * @var float Cantidad del apunte
     */
    public $amount;

    /**
     * @var int Cuenta contable
     */
    public $account;

    const TYPE_DEBE = 'D';
    const TYPE_HABER = 'H';

    /**
     * Da formato a un número en función de la convención de Keme
     *
     * @return string Número formateado
     */
    public function getAmount()
    {
        return number_format(abs($this->amount), 2, '.', '');
    }

    /**
     * Fija la posición del apunte en función de su valor
     *
     * @param bool $invert Invertir el resultado?
     * @return string debe o haber
     */
    public function calcType($invert = false)
    {
        if ($this->amount > 0) {
            if ($invert) {
                $type = Apunte::TYPE_DEBE;
            } else {
                $type = Apunte::TYPE_HABER;
            }
        } else {
            if ($invert) {
                $type = Apunte::TYPE_HABER;
            } else {
                $type = Apunte::TYPE_DEBE;
            }
        }

        $this->type = $type;
    }

}