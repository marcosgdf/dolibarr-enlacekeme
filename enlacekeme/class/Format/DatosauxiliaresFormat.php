<?php

namespace Marcosgdf\EnlaceKeme\Format;

class DatosauxiliaresFormat
{
    /**
     * @var string Código cuenta
     */
    public $cuenta;
    /**
     * @var string Razón social
     */
    public $razon_social;
    /**
     * @var string Nombre comercial
     */
    public $nombre_comercial;
    /**
     * @var string NIF/CIF
     */
    public $nif;
    /**
     * @var string Página web
     */
    public $web;
    /**
     * @var string Domicilio
     */
    public $domicilio;
    /**
     * @var string Población
     */
    public $poblacion;
    /**
     * @var string Código postal
     */
    public $cp;
    /**
     * @var string Provincia
     */
    public $provincia;
    /**
     * @var string Teléfono
     */
    public $telefono;
    /**
     * @var string País
     */
    public $pais;
    /**
     * @var string Fax
     */
    public $fax;
    /**
     * @var string Email
     */
    public $email;
    /**
     * @var string Observaciones
     */
    public $observaciones;
    /**
     * @var string Código Cuenta Cliente
     */
    public $ccc;
    /**
     * @var string IBAN
     */
    public $iban;
    /**
     * @var string BIC
     */
    public $bic;
    /**
     * @var string Sufijo (referente a domiciliaciones bancarias)
     */
    public $sufijo;
    /**
     * @var bool Pagos domiciliables?
     */
    public $domiciliable = false;
    /**
     * @var string Referencia Única Mandato (SEPA)
     */
    public $rum;
    /**
     * @var \DateTime Fecha de firma de mandato
     */
    public $fecha_firma_mandato;
    /**
     * @var string Clave país
     */
    public $clave_pais;
    /**
     * @var string Clave Id Fiscal
     */
    public $clave_idfiscal;

    public function __toString()
    {
        $return = CommonFormat::fill($this->cuenta, 30);
        //Razón/denominación social
        $return .= CommonFormat::fill($this->razon_social, 80);
        //Nombre comercial
        $return .= CommonFormat::fill($this->nombre_comercial, 80);
        //CIF/NIF
        $return .= CommonFormat::fill($this->nif, 40);
        //Página web
        $return .= CommonFormat::fill($this->web, 80);
        //Domicilio
        $return .= CommonFormat::fill($this->domicilio, 80);
        //Población
        $return .= CommonFormat::fill($this->poblacion, 80);
        //Código postal
        $return .= CommonFormat::fill($this->cp, 40);
        //Provincia
        $return .= CommonFormat::fill($this->provincia, 80);
        //Teléfono
        $return .= CommonFormat::fill($this->telefono, 80);
        //País
        $return .= CommonFormat::fill($this->pais, 80);
        //Fax
        $return .= CommonFormat::fill($this->fax, 80);
        //Email
        $return .= CommonFormat::fill($this->email, 80);
        //Observaciones
        $return .= CommonFormat::fill($this->observaciones, 166);
        //CCC
        $return .= CommonFormat::fill($this->ccc, 80);
        //IBAN
        $return .= CommonFormat::fill($this->iban, 40);
        //BIC
        $return .= CommonFormat::fill($this->bic, 20);
        //Sufijo
        $return .= CommonFormat::fill($this->sufijo, 3);
        //Domiciliable
        $return .= $this->domiciliable ? 1 : 0;
        //RUM
        $return .= CommonFormat::fill($this->rum, 35);
        //Fecha firma mandato
        if ($this->fecha_firma_mandato) {
            $return .= $this->fecha_firma_mandato->format('d-m-Y');
        } else {
            $return .= str_repeat(' ', 10);
        }
        //Clave país
        $return .= CommonFormat::fill($this->clave_pais, 2);
        //Clave id fiscal
        $return .= CommonFormat::fill($this->clave_idfiscal, 1);

        return $return;
    }
}