<?php

namespace Marcosgdf\EnlaceKeme\Format;

class InvoiceFormat
{
    /**
     * @var int Apunte en diario
     */
    public $apunte;
    /**
     * @var string Cuenta base iva
     */
    public $cuenta_iva;
    /**
     * @var float Base imponible
     */
    public $bi;
    /**
     * @var string Clave IVA
     */
    public $iva_clave = '';
    /**
     * @var float Tipo IVA (%)
     */
    public $iva_tx;
    /**
     * @var float Tipo Recargo Equival.(%)
     */
    public $re_tx;
    /**
     * @var float Cuota IVA
     */
    public $iva_amount;
    /**
     * @var float Cuota REC
     */
    public $re_amount;
    /**
     * @var string Cuenta de factura
     */
    public $cuenta_factura;
    /**
     * @var \DateTime Fecha de factura
     */
    public $fecha_factura;
    /**
     * @var bool ¿ Es soportado ?
     */
    public $soportado = false;
    /**
     * @var bool ¿ Es AIB ?
     */
    public $aib = false;
    /**
     * @var bool ¿ Es A.I. Servicios ?
     */
    public $ais = false;
    /**
     * @var bool ¿ Adq. serv. extranjero?
     */
    public $ase = false;
    /**
     * @var bool ¿ Prestación serv. UE ?
     */
    public $pis = false;
    /**
     * @var bool ¿ Bien inversión ?
     */
    public $inversion = false;
    /**
     * @var bool ¿ Es EIB ?
     */
    public $eib = false;
    /**
     * @var \DateTime Fecha operación
     */
    public $fechaop;
    /**
     * @var bool ¿ EXPORTACIÓN?
     */
    public $exportacion = false;

    public function __toString()
    {
        //Apunte en el diario de la compra
        $string = CommonFormat::fill($this->apunte, 15);
        //Cuenta base IVA
        $string .= CommonFormat::fill((string)$this->cuenta_iva, 30);
        //Base imponible
        $string .= CommonFormat::fill((float)$this->bi, 17);
        //Clave IVA
        $string .= CommonFormat::fill((string) $this->iva_clave, 15);
        //Tipo IVA (%)
        $string .= CommonFormat::fill((float)abs($this->iva_tx), 5);
        //Tipo Recargo equiv. (%)
        $string .= CommonFormat::fill((float)abs($this->re_tx), 5);
        //Cuota IVA
        $string .= CommonFormat::fill((float)$this->iva_amount, 17);
        //Cuota REC
        $string .= CommonFormat::fill((float)$this->re_amount, 17);
        //Cuenta de factura
        $string .= CommonFormat::fill((string)$this->cuenta_factura, 30);
        //Fecha de factura
        $string .= $this->fecha_factura->format('d-m-Y');
        //¿Es soportado?
        $string .= ($this->soportado ? '1' : '0');
        //¿Es AIB?
        $string .= ($this->aib ? '1' : '0');
        //¿Es AIS?
        $string .= ($this->ais ? '1' : '0');
        //¿Es AS no UE?
        $string .= ($this->ase ? '1' : '0');
        //¿Es PIS?
        $string .= ($this->pis ? '1' : '0');
        //¿Bien de inversión?
        $string .= ($this->inversion ? '1' : '0');
        //¿Es EIB?
        $string .= ($this->eib ? '1' : '0');
        //Fecha operación
        $string .= $this->fechaop->format('d-m-Y');
        //Exportación
        $string .= ($this->exportacion ? '1' : '0');

        return $string;
    }

}