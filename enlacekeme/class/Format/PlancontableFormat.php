<?php

namespace Marcosgdf\EnlaceKeme\Format;

class PlancontableFormat
{
    public $cuenta;
    public $nombre;

    public function __toString()
    {
        //Número de cuenta
        $return = CommonFormat::fill($this->cuenta, 30);
        //Nombre de la cuenta
        $return .= CommonFormat::fill($this->nombre, 80);

        return $return;
    }
}