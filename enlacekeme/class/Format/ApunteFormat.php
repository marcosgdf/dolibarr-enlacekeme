<?php

namespace Marcosgdf\EnlaceKeme\Format;

/**
 * Clase que representa un apunte en el formato de Keme contabilidad
 *
 * @package Marcosgdf\EnlaceKeme\Format
 */
class ApunteFormat
{
    /**
     * @var int Número de asiento
     */
    public $numero;
    /**
     * @var int Número de apunte
     */
    public $apunte;
    /**
     * @var string Código diario
     */
    public $diario = '';
    /**
     * @var \DateTime Fecha del asiento
     */
    public $fecha;
    /**
     * @var string Cuenta contable
     */
    public $cuenta;
    /**
     * @var string Concepto del asiento
     */
    public $concepto;
    /**
     * @var float Cantidad en el debe
     */
    public $debe = 0;
    /**
     * @var float Cantidad en el haber
     */
    public $haber = 0;
    /**
     * @var string Documento contable
     */
    public $documento = '';
    /**
     * @var string Código de factura
     */
    public $codfactura = '';

    /**
     * Convierte un objeto asiento en el formato adecuado para Keme contabilidad
     *
     * @return string Asiento formateado
     */
    public function __toString()
    {
        if ($this->debe && $this->haber) {
            throw new \RuntimeException('El debe y el haber no pueden estar definidos a la vez');
        }

        //Número
        $string = CommonFormat::fill($this->numero, 15);
        //Apunte
        $string .= CommonFormat::fill($this->apunte, 15);
        //Diario
        $string .= CommonFormat::fill($this->diario, 40);
        //Fecha
        $string .= $this->fecha->format('d-m-Y');
        //Código cuenta
        $string .= CommonFormat::fill((string)$this->cuenta, 30);
        //Concepto cargo/abono
        $string .= CommonFormat::fill($this->concepto, 100);
        //Debe
        $string .= CommonFormat::fill((float) $this->debe, 17);
        //Haber
        $string .= CommonFormat::fill((float) $this->haber, 17);
        //Documento
        $string .= CommonFormat::fill((string) $this->documento, 80);
        //Cód. Factura
        $string .= CommonFormat::fill((string) $this->codfactura, 80);

        return $string;
    }
}