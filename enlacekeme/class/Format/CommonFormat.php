<?php

namespace Marcosgdf\EnlaceKeme\Format;

class CommonFormat
{
    /**
     * Alias de str_pad con compatibilidad con UTF-8
     *
     * @param int|float|string $string Cadena de origen
     * @param int $length Longitud establecida
     * @return string
     */
    public static function fill($string, $length)
    {
        mb_internal_encoding('UTF-8');

        $modif = null;

        //Los campos de tipo numérico se rellenan con 0 al principio.
        if (is_int($string)) {
            $fill = '0';
            $modif = STR_PAD_LEFT;
        } elseif (is_float($string)) {
            $fill = '0';
            $modif = STR_PAD_LEFT;

            $string = number_format($string, 2, '.', '');
        } elseif (is_string($string) || !$string) {
            $fill = ' ';
        } else {
            throw new \InvalidArgumentException('El valor de $string no es int, float o string');
        }

        $str_length = mb_strlen($string);
        $str_diff = $length - $str_length;

        if ($str_diff > 0) {
            if ($modif === STR_PAD_LEFT) {
                return str_repeat($fill, $str_diff).$string;
            } else {
                return $string.str_repeat($fill, $str_diff);
            }

        } else {
            return mb_substr($string, 0, $length);
        }
    }
}
