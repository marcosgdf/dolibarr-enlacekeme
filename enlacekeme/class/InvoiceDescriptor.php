<?php

namespace Marcosgdf\EnlaceKeme;

class InvoiceDescriptor
{
    /**
     * @var string Fecha de la factura
     */
    public $date;
    /**
     * @var string Referencia de la factura
     */
    public $ref;
    /**
     * @var int Tipo de factura. Ver constantes
     */
    public $type;
    /**
     * @var float Total antes de impuestos
     */
    public $total_ht = 0;
    /**
     * @var float Total después de impuestos
     */
    public $total_ttc = 0;
    /**
     * @var array Bases de IVA
     */
    public $bases = array();
    /**
     * @var array Array describiendo el IVA de la factura
     */
    public $iva = array();
    /**
     * @var array Recargos de equivalencia
     */
    public $localtax1 = array();
    /**
     * @var array Retenciones IRPF
     */
    public $localtax2 = array();
    /**
     * @var int Factura de origen (solo para las rectificativas)
     */
    public $source;
    /**
     * @var int|false Tipo de transacción europea. Comprobar las constantes EUINVOICE_PRODUCTS y EUINVOICE_SERVICES
     */
    public $eu_transaction = false;

    const TYPE_ANTICIPO = 3;
    const TYPE_RECTIFICATIVA = 1;
    const TYPE_GENERAL = 0;
    const TYPE_ABONO = 2;

    const EUINVOICE_PRODUCTS = 0;
    const EUINVOICE_SERVICES = 1;

    /**
     * Invertimos el orden de los signos del array de diferencia
     */
    public function invert()
    {
        $absArray = function($array) use (&$absArray) {

            foreach ($array as $key => &$val) {

                if (is_array($val)) {
                    $val = $absArray($val);
                } elseif ($key != 'cuenta_base' && !is_bool($val)) {
                    $val = -$val;
                }
            }

            return $array;
        };

        foreach ($this as $key => &$val) {

            if (is_array($val)) {
                $val = $absArray($val);
            } elseif ($val !== null && (!is_bool($val))) {
                $val = -$val;
            }
        }
    }
}