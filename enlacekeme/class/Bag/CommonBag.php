<?php

namespace Marcosgdf\EnlaceKeme\Bag;

class CommonBag
{
    public $prefix;
    public $contents = array();

    public function __construct($prefix)
    {
        $this->prefix = $prefix;
    }

    public function add($object)
    {
        $this->contents[] = $object;
    }

    public function __toString()
    {
        $return = $this->prefix."\n";

        foreach ($this->contents as $content) {
            $return .= $content."\n";
        }

        return $return;
    }

}