<?php

require_once DOL_DOCUMENT_ROOT.'/core/lib/admin.lib.php';

class ActionsEnlaceKeme
{

    public function doActions($parameters, &$object, &$action, $hookmanager)
    {
        if ($object instanceof Product) {

            if ($action == 'setaccountancy_code_sell') {
                $_POST['accountancy_code_sell'] = $this->formatAccount(GETPOST('accountancy_code_sell'));
            } elseif ($action == 'setaccountancy_code_buy') {
                $_POST['accountancy_code_buy'] = $this->formatAccount(GETPOST('accountancy_code_buy'));
            }
        } elseif ($object instanceof Societe) {

            if (GETPOST('acreedor', 'string')) {
                $object->acreedor = true;
            } else {
                $object->acreedor = false;
            }
        }
    }

    private function formatAccount($string)
    {

        global $db, $conf;

        if (!$longitud = dolibarr_get_const($db, 'ENLACEKEME_CUENTA_LONGITUD', $conf->entity)) {
            return $string;
        }

        $blocks = explode('.', $string);

        if (count($blocks) < 2) {
            return $string;
        } else {

            return $blocks[0].str_repeat('0', ($longitud - strlen($blocks[0].$blocks[1]))).$blocks[1];

        }

    }

    public function formObjectOptions($parameters, &$object, &$action, HookManager $hookManager)
    {

        if ($object instanceof Societe) {

            if ($action == 'create') {

                $checked = ($object->acreedor ? ' checked' : '');

                print '<tr><td><span class="fieldrequired">Acreedor</span></td><td colspan="3"><input type="checkbox" name="acreedor" value="on"'.$checked.'/></td></tr>';
            }

        }
    }
}