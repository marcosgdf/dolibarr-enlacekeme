<?php

if (!file_exists('../../main.inc.php')) {
    require_once '../../../main.inc.php';
} else {
    require_once '../../main.inc.php';
}

if (!file_exists('../../core/lib/admin.lib.php')) {
    require_once '../../../core/lib/admin.lib.php';
} else {
    require_once '../../core/lib/admin.lib.php';
}

require '../class/EnlaceKeme.class.php';

use Marcosgdf\EnlaceKeme\EnlaceKeme;

$relaciones = EnlaceKeme::obtenerRelacionesIVA();

if ($_POST) {

    $error = 0;
    $refs = GETPOST('ref');
    $refs_re = GETPOST('ref_re');

    foreach ($refs as $id => $ref) {
        if (isset($relaciones[$id])) {

            if ((strlen($ref) > 5) || (strlen($refs_re[$id]) > 5)) {
                setEventMessage('Las referencias de KEME no pueden tener más de 5 caracteres.', 'errors');
                $error++;
                continue;
            }

            if (!EnlaceKeme::actualizarRelacionIVA($id, $ref, $refs_re[$id])) {
                setEventMessage('Ha ocurrido un error al intentar grabar la relación '.$ref.'.', 'errors');
                continue;
            }

            //Actualizamos las referencias
            $relaciones[$id]['ref'] = $ref;
            $relaciones[$id]['ref_re'] = $refs_re[$id];

            //Actualizamos las cuentas contables
            $freetext = GETPOST('cuenta_re_purchases_'.$id);

            dolibarr_set_const($db, "ENLACEKEME_CUENTA_RE_SELL_".$id, $freetext, 'chaine', 0, '', $conf->entity);

            $freetext = GETPOST('cuenta_re_sales_'.$id);

            dolibarr_set_const($db, "ENLACEKEME_CUENTA_RE_BUY_".$id, $freetext, 'chaine', 0, '', $conf->entity);
        }
    }

    $toset = array(
        "ENLACEKEME_CUENTA_HAEAT_IRPF",
        "ENLACEKEME_CUENTA_HAEAT_IVA",
        "ENLACEKEME_PREFIJOCUENTA_ANTICIPOS",
        "ENLACEKEME_CUENTA_LONGITUD",
        "ENLACEKEME_CUENTA_RET_IRPF",
        "ENLACEKEME_CUENTA_DEVO_COMPRAS",
        "ENLACEKEME_CUENTA_DEVO_VENTAS"
    );

    foreach ($toset as $set) {
        dolibarr_set_const($db, $set, GETPOST($set), 'chaine', 0, '', $conf->entity);
    }

    if (!$error) {
        setEventMessage('Configuración guardada correctamente.');
    }

}

$title = 'Configuración del módulo Enlace Keme-Contabilidad';

llxHeader('', $title);

$langs->load('admin');

$linkback = '<a href="'.DOL_URL_ROOT.'/admin/modules.php">'.$langs->trans("BackToModuleList").'</a>';
print_fiche_titre($title, $linkback, 'setup');

print '<br />';

print '<div class="titre">Relación de códigos de IVA Dolibarr - KEME</div>';
print '<table class="noborder" width="100%">';
print '<tr class="liste_titre">';
print '<td>'.$langs->trans("Tipo de IVA").'</td>';
print '<td>'.$langs->trans("Código KEME").'</td>';
print '<td>'.$langs->trans('RE asociado').'</td>';
print '<td>'.$langs->trans('Código KEME').'</td>';
print '<td>'.$langs->trans('Cuenta RE ventas').'</td>';
print '<td>'.$langs->trans('Cuenta RE compras').'</td>';
print '<td>'.$langs->trans('Nota').'</td>';
print '<td style="width: 60px"></td>';
print '</tr>';

print '<form method="POST" action="">';

foreach ($relaciones as $id => $relacion) {

    print '<tr>
        <td>'.$relacion['rate'].'</td>
        <td><input type="text" value="'.$relacion['ref'].'" name="ref['.$id.']" maxlength="5"></td>
        <td>'.$relacion['re'].'</td>
        <td><input type="text" value="'.$relacion['ref_re'].'" name="ref_re['.$id.']" maxlength="5"></td>
        <td><input type="text" value="'.$conf->global->{'ENLACEKEME_CUENTA_RE_SELL_'.$id}.'" name="cuenta_re_purchases_'.$id.'"></td>
        <td><input type="text" value="'.$conf->global->{'ENLACEKEME_CUENTA_RE_BUY_'.$id}.'" name="cuenta_re_sales_'.$id.'"></td>
        <td>'.$relacion['note'].'</td>
    </tr>';
}

print '</table>';

print '<br /><br />';

print '<div class="titre">Otros parámetros</div>';
print '<table class="noborder" width="100%">';
print '<tr class="liste_titre"><td>Parámetro</td><td>Valor</td></tr>';

print '<tr><td>Longitud de cuentas contables</td><td><input type="text" name="ENLACEKEME_CUENTA_LONGITUD" value="'.$conf->global->ENLACEKEME_CUENTA_LONGITUD.'"></td></tr>';
print '<tr><td>Prefijo para generación de cuenta contable de anticipos de clientes<br />La generación sera: {prefijo}+{cód cliente}. Ejemplo: <strong>438</strong>000001</td><td><input type="text" name="ENLACEKEME_PREFIJOCUENTA_ANTICIPOS" value="'.$conf->global->ENLACEKEME_PREFIJOCUENTA_ANTICIPOS.'"></td></tr>';

print '</table>';
print '<br /><br />';

print '<div class="titre">Cuentas contables</div>';
print '<table class="noborder" width="100%">';
print '<tr class="liste_titre"><td>Parámetro</td><td>Valor</td></tr>';

print '<tr><td>Hacienda Pública, retenciones y pagos a cuenta</td><td><input type="text" name="ENLACEKEME_CUENTA_RET_IRPF" value="'.$conf->global->ENLACEKEME_CUENTA_RET_IRPF.'"></td></tr>';
print '<tr><td>Hacienda Pública acreedora por retenciones practicadas</td><td><input type="text" name="ENLACEKEME_CUENTA_HAEAT_IRPF" value="'.$conf->global->ENLACEKEME_CUENTA_HAEAT_IRPF.'"></td></tr>';
print '<tr><td>Hacienda Pública acreedora por IVA</td><td><input type="text" name="ENLACEKEME_CUENTA_HAEAT_IVA" value="'.$conf->global->ENLACEKEME_CUENTA_HAEAT_IVA.'"></td></tr>';
print '<tr><td>Devoluciones de compras de mercaderías</td><td><input type="text" name="ENLACEKEME_CUENTA_DEVO_COMPRAS" value="'.$conf->global->ENLACEKEME_CUENTA_DEVO_COMPRAS.'"></td></tr>';
print '<tr><td>Devoluciones de ventas de mercaderías</td><td><input type="text" name="ENLACEKEME_CUENTA_DEVO_VENTAS" value="'.$conf->global->ENLACEKEME_CUENTA_DEVO_VENTAS.'"></td></tr>';

print '</table>';


print '<br /><br /><div style="text-align: center"><input type="submit" value="Actualizar" class="button"></div></form>';

dol_fiche_end();

llxFooter();
$db->close();