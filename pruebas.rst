================
Pruebas que se deben realizar:
================

Globales
--------------
* Panel de administración
* Validación de longitud de cuentas
* Autocompletado de cuentas

Facturas a clientes
--------------
* Factura a cliente normal
* Factura a cliente normal con descuentos
* Factura a cliente normal con cargo a descuento global
* Factura a cliente rectificativa
* Factura a cliente de abono

Facturas de proveedores
--------------
* Factura de proveedor normal
* Factura de proveedor normal con descuentos
* Factura de proveedor normal con cargo a descuento global (¿?)

Líneas de factura
--------------
* Línea normal
* Línea con RE
* Línea con diferentes tipos de IVA
* Línea con IRPF

Pagos de facturas a clientes
--------------
* Pago en efectivo
* Pago por transferencia
* Varios pagos contra una factura
* Varios pagos contra una factura y parte pendiente

Pagos de facturas de proveedores
--------------
* Pago en efectivo
* Pago por transferencia
* Varios pagos contra una factura
* Varios pagos contra una factura y parte pendiente

Pagos de impuestos
--------------
* Pago de IVA
* Pago de IRPF

Terceros
--------------
* Correcta exportación de datos de clientes
* Correcta exportación de datos de proveedores